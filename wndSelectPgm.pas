unit wndSelectPgm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, uHBoardUtils, ImgList;

  {*
  20161206 : A Faire
     - Envoi des fichiers vers la cible
  *}

type

  TfrmSelectPgm = class(TForm)
    bbtnClose: TBitBtn;
    lbArchives: TListBox;
    ImageList: TImageList;
    bbtnEdit: TBitBtn;
    bbtnDetail: TBitBtn;
    lblUnit: TLabel;
    stUnitName: TStaticText;
    procedure lbArchivesClick(Sender: TObject);
    procedure bbtnEditClick(Sender: TObject);
    procedure lbArchivesDblClick(Sender: TObject);
    procedure lbArchivesDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure bbtnDetailClick(Sender: TObject);
  private
    fStoreParameters : TStoresParameters;
    fParameters : TParameters;
    fArchivesStatus : TStringList;
    fUnitDetail : TUnit;
    fUnitPath: String;
    procedure LoadArchives();
    procedure SetUnitDetail(const _Value: TUnit);
    procedure SetUnitPath(const _Value: String);
    procedure ControlButtonsState;
  public
    constructor Create(_Owner : TComponent; _Parameters : TParameters; _UnitDetail : TUnit; _UnitPath : String); overload;
    destructor Destroy(); override;
    property UnitDetail : TUnit read fUnitDetail write SetUnitDetail;
    property UnitPath : String read fUnitPath write SetUnitPath;
  end;

var
  frmSelectPgm: TfrmSelectPgm;

implementation

uses wndDetail, wndEditPgm, uCommon;

{$R *.dfm}

constructor TfrmSelectPgm.Create(_Owner : TComponent; _Parameters : TParameters; _UnitDetail : TUnit; _UnitPath : String);
begin
  Inherited Create(_Owner);
  fParameters := _Parameters;
  fArchivesStatus := TStringList.Create();
  fStoreParameters := TStoresParameters.Create(_UnitPath);
  UnitDetail := _UnitDetail;
  UnitPath := _UnitPath;
end;

destructor TfrmSelectPgm.Destroy();
begin
  fArchivesStatus.Free();
  fStoreParameters.Free();
  Inherited Destroy();
end;

procedure TfrmSelectPgm.SetUnitDetail(const _Value: TUnit);
begin
  fUnitDetail := _Value;
  stUnitName.Caption := fUnitDetail.UnitName;
end;

procedure TfrmSelectPgm.SetUnitPath(const _Value: String);
begin
  FUnitPath := _Value;
  LoadArchives();
end;

procedure TfrmSelectPgm.LoadArchives();
var
  nStatus,
  nCurrentStatus,
  nCountFiles,
  nCountStores : Integer;
begin
  LoadFiles(FUnitPath, lbArchives.Items, '*.zip');
  fArchivesStatus.Clear();
  for nCountFiles := 0 to lbArchives.Count-1 do
  begin
    nStatus := CST_OK;
    for nCountStores := 0 to fStoreParameters .StoresDataLocation.Count-1 do
    begin
      nCurrentStatus := GetArchiveStatus(fStoreParameters.StoresDataLocation[nCountStores], lbArchives.Items[nCountFiles]);
      if (nCurrentStatus = CST_SND) and (nStatus = CST_OK) then nStatus := CST_SND
      else if (nCurrentStatus = CST_KO) then nStatus := CST_KO;
    end;
    fArchivesStatus.Add(IntToStr(nStatus));
  end;
end;

procedure TfrmSelectPgm.ControlButtonsState();
begin
  bbtnEdit.Enabled := lbArchives.ItemIndex > -1;
  bbtnDetail.Enabled := lbArchives.ItemIndex > -1;
end;

procedure TfrmSelectPgm.lbArchivesClick(Sender: TObject);
begin
  ControlButtonsState();
end;

procedure TfrmSelectPgm.bbtnEditClick(Sender: TObject);
var
  frmEditPgm : TfrmEditPgm;
begin
  frmEditPgm := TfrmEditPgm.Create( Self,
                                    fParameters,
                                    UnitDetail,
                                    UnitPath,
                                    ActualValue(lbArchives)
                                   );
  frmEditPgm.ShowModal();
  frmEditPgm.Free();
  LoadArchives();
  ControlButtonsState();
end;

procedure TfrmSelectPgm.lbArchivesDblClick(Sender: TObject);
begin
  bbtnEditClick(nil);
end;

procedure TfrmSelectPgm.lbArchivesDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  bmpDisplay :TBitmap;
begin
  bmpDisplay := TBitmap.Create;
  if Index < fArchivesStatus.Count then ImageList.GetBitmap(StrToInt(fArchivesStatus[Index]),bmpDisplay);
  with (Control as TListBox) do
  begin
    Canvas.FillRect(Rect);
    Canvas.TextOut(Rect.Left+ImageList.Height+2,Rect.Top,Items[Index]);
    Canvas.Draw(Rect.Left+1,Rect.Top+1,bmpDisplay);
  end;
  bmpDisplay.Free;
end;

procedure TfrmSelectPgm.bbtnDetailClick(Sender: TObject);
var
  frmDetail : TfrmDetail;
begin
  frmDetail := TfrmDetail.Create( Self,
                                  fParameters,
                                  fStoreParameters,
                                  UnitDetail,
                                  UnitPath,
                                  ActualValue(lbArchives)
                                );
  frmDetail.ShowModal();
  frmDetail.Free();
  ControlButtonsState();
end;

end.
