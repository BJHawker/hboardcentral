unit uZipUtils;

interface

uses AbZipTyp, AbArcTyp, AbUtils, AbUnzPrc, AbZipPrc, StdCtrls, Classes;

type
  TZipHelper = class
  private
    logFile : TextFile;
    FLogFileName : String;
    FOutputLabel : TLabel;
    procedure PrepareLog(_Archive : TAbZipArchive);
    procedure EndLog();
    function IsLogFile: Boolean;
    function IsLogLabel: Boolean;
  public
    constructor Create();
    destructor Destroy; override;
    procedure UnzipProc( Sender : TObject; Item : TAbArchiveItem; const NewName : string );
    procedure ZipProc( Sender : TObject; Item : TAbArchiveItem; OutStream : TStream );
    procedure ArchiveItemProgress( Sender: TObject; Item: TAbArchiveItem; Progress: Byte; var Abort: Boolean);
    property LogFileName : String read FLogFileName write FLogFileName;
    property OutputLabel : TLabel read FOutputLabel write FOutputLabel;
  end;

implementation

uses uCommon, Forms, SysUtils;

procedure TZipHelper.ArchiveItemProgress( Sender: TObject; Item: TAbArchiveItem; Progress: Byte; var Abort: Boolean);
type
  TMethodStrings = array [ cmStored..cmDCLImploded ] of string;
const
  MethodStrings : TMethodStrings = ('UnStoring', 'UnShrinking', 'UnReducing',
                                    'UnReducing', 'UnReducing', 'UnReducing',
                                    'Exploding', 'DeTokenizing', 'Inflating',
                                    'Enhanced Inflating', 'DCL Exploding');
var
  ActionString : string;
  CompMethod: TAbZipCompressionMethod;
begin
  case Item.Action of
    aaAdd : ActionString := 'Adding  ';
    aaFreshen : ActionString := 'Freshening  ';
    else begin
      CompMethod := (Item as TAbZipItem).CompressionMethod;
      if CompMethod in [cmStored..cmDCLImploded] then
        ActionString := MethodStrings[(Item as TAbZipItem).CompressionMethod] +
          '  '
      else
        ActionString := 'Decompressing  ';
    end;
  end;
  if IsLogFile() then WriteLn(logFile, ActionString + Item.FileName );
  if IsLogLabel() then
  begin
    FOutputLabel.Caption := ActionString + ExtractOnlyFileName( ReplaceString(Item.FileName, '/', '\') );
    Application.ProcessMessages();
  end;
end;

constructor TZipHelper.Create();
begin
  inherited Create;
end;

Destructor TZipHelper.Destroy;
begin
  inherited Destroy;
end;

function TZipHelper.IsLogFile() : Boolean;
begin
  Result := not (FLogFileName = '');
end;

function TZipHelper.IsLogLabel() : Boolean;
begin
  Result := not (FOutputLabel = nil);
end;

procedure TZipHelper.PrepareLog(_Archive : TAbZipArchive);
begin
  if IsLogFile() then
  begin
    AssignFile(logFile, FLogFileName);
    if FileExists(FLogFileName) then
      Append(logFile)
    else
      Rewrite(logFile);
  end;
end;

procedure TZipHelper.EndLog();
begin
  if IsLogFile() then CloseFile(logFile);
end;

procedure TZipHelper.UnzipProc( Sender : TObject; Item : TAbArchiveItem; const NewName : string );
begin
  PrepareLog(TAbZipArchive(Sender));
  AbUnzip( Sender, TAbZipItem(Item), NewName);
  EndLog();
end;

procedure TZipHelper.ZipProc( Sender : TObject; Item : TAbArchiveItem;OutStream : TStream );
begin
  PrepareLog(TAbZipArchive(Sender));
  AbZip( TAbZipArchive(Sender), TAbZipItem(Item), OutStream );
  EndLog();
end;

end.
