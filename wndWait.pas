unit wndWait;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls;

type

  TfrmWait = class(TForm)
    lblMain: TLabel;
    tmrBlink: TTimer;
    lblMessage: TLabel;
    procedure tmrBlinkTimer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { D�clarations priv�es }
  public
    constructor Create(AOwner: TComponent; _Message : String); overload;
  end;

var
  frmWait: TfrmWait;

implementation

{$R *.dfm}

constructor TfrmWait.Create(AOwner: TComponent; _Message : String);
begin
  inherited Create(AOwner);
  lblMain.Caption := _Message;
end;

procedure TfrmWait.tmrBlinkTimer(Sender: TObject);
begin
  lblMain.Visible := not lblMain.Visible;
  Application.ProcessMessages();
end;

procedure TfrmWait.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := False;
end;

end.
