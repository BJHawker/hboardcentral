unit wndZipAndUnzip;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, AbMeter, AbZipper, AbBase, AbBrowse,
  AbZBrows, AbUnzper;

type

  TfrmZipAndUnzip = class(TForm)
    lblMain: TLabel;
    tmrBlink: TTimer;
    lblMessage: TLabel;
    UnZipper: TAbUnZipper;
    Zipper: TAbZipper;
    AbMeter1: TAbMeter;
    AbMeter2: TAbMeter;
    procedure tmrBlinkTimer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { D�clarations priv�es }
  public
    procedure CreateArchive(_SourcePath, _DestPath, _ArchiveName : String; _Message : String);
    function LoadArchive(_ArchiveName, _DestDir : String; _Message : String) : Boolean;
  end;

var
  frmZipAndUnzip: TfrmZipAndUnzip;

implementation

{$R *.dfm}

uses uCommon;

procedure TfrmZipAndUnzip.tmrBlinkTimer(Sender: TObject);
begin
  lblMain.Visible := not lblMain.Visible;
  Application.ProcessMessages();
end;

procedure TfrmZipAndUnzip.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := False;
end;

procedure TfrmZipAndUnzip.CreateArchive(_SourcePath, _DestPath, _ArchiveName : String; _Message : String);
var
  sLogFileName,
  sFileName : String;
begin
  lblMain.Caption := _Message;
  Show();
  Application.ProcessMessages();
  sLogFileName := _ArchiveName + '.log';
  sFileName := _ArchiveName;
  try
    if FileExists(sLogFileName) then DeleteFile(sLogFileName);
    if FileExists(sFileName) then DeleteFile(sFileName);
    Zipper.BaseDirectory := _SourcePath;
    Zipper.LogFile := sLogFileName;
    Zipper.Filename := sFileName;
    Zipper.AddFiles('*.*', 0 );
  except
    if MessageDlg('L''archive de sauvegarde n''a pu �tre finalis�e ('+_ArchiveName+'), Voulez vous annuler ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then Application.Terminate;
  end;
  Close();
end;

function TfrmZipAndUnzip.LoadArchive(_ArchiveName, _DestDir : String; _Message : String) : Boolean;
begin
  Result := false;
  lblMain.Caption := _Message;
  Show();
  Application.ProcessMessages();
  try
    Unzipper.FileName := _ArchiveName;
    Unzipper.BaseDirectory := _DestDir;
    Unzipper.ExtractFiles( '*.*' );
    Result := True;
  except
    if MessageDlg('L''archive de sauvegarde n''a pu �tre finalis�e ('+_ArchiveName+'), Voulez vous annuler ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then Application.Terminate;
  end;
  Close();
end;

end.
