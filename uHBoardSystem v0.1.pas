unit uHBoardSystem;

interface

uses Classes;

type
  TPlan = class(TObject)
    FDay : Integer;
    FHour : TDateTime;
    FFolder : String;
  public
    constructor Create(_Day: Integer; _Hour: TDateTime; _Folder: String); overload;
    property Day : Integer read FDay write FDay;
    property Hour : TDateTime read FHour write FHour;
    property Folder : String read FFolder write FFolder;
  end;

  TPlans = class(TList)
  private
    FPath: String;
    procedure SetPath(const Value: String);
    procedure ReadPlan(_PlanFileName : String);
    function GetPlan(_Index : Integer) : TPlan;
  public
    destructor Destroy; override;
    procedure Clear; override;
    function Add(_Plan : TPlan) : Integer; overload;
    property Path: String read FPath write SetPath;
    property Plan[Index:Integer]:TPlan read GetPlan; default;
  end;

implementation

uses SysUtils;

Const
  CST_PLAN_FILENAME = 'plan.csv';

{ Commons }
function LoadCSVInStrings(_FileName: string): TStringList;
var
  tsResult : TStringList;
begin
  tsResult:=TStringList.Create();
  tsResult.LoadFromFile(_FileName);
  if tsResult.Count>0 then
    while tsResult[tsResult.Count-1]='' do tsResult.Delete(tsResult.Count-1);
  result := tsResult;
end;

function GetCSVcell(_Line: string; _Index: integer): string;
var
  tsResult:TStringList;
begin
  tsResult:=TStringList.Create();
  tsResult.Text:=StringReplace(_Line,';',#13#10,[rfReplaceAll]);
  try
    result:=tsResult[_Index];
  except
    result:='';
  end;
  tsResult.Free();
end;

{ TPlans }

destructor TPlans.Destroy;
begin
  Clear;
  inherited;
end;

function TPlans.GetPlan(_Index: Integer): TPlan;
begin
  result := TPlan(Items[_Index]);
end;

procedure TPlans.Clear;
var
  nCount : Integer;
begin
  for nCount := Count-1 downto 0 do Plan[nCount].Free();
  inherited;
end;

procedure TPlans.ReadPlan(_PlanFileName : String);
var
  slReadPlans : TStringList;
  nCount : Integer;
begin
  Clear();
  if FileExists(_PlanFileName) then
  begin
    slReadPlans := LoadCSVinStrings(_PlanFileName);
    for nCount := 1 to slReadPlans.Count-1 do
      Add(
        TPlan.Create(
          StrToInt(GetCSVcell(slReadPlans[nCount], 0)),
          StrToTime(GetCSVcell(slReadPlans[nCount], 1)),
          GetCSVcell(slReadPlans[nCount], 2)
          )
        );
    slReadPlans.Free();
  end;
end;

procedure TPlans.SetPath(const Value: String);
begin
  FPath := Value;
  ReadPlan(FPath + '\' + CST_PLAN_FILENAME);
end;

function TPlans.Add(_Plan: TPlan): Integer;
begin
  result := inherited Add(_Plan);
end;

{ TPlan }

constructor TPlan.Create(_Day: Integer; _Hour: TDateTime; _Folder: String);
begin
  inherited Create();
  Day := _Day;
  Hour := _Hour;
  Folder := _Folder;
end;

end.
