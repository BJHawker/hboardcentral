unit wndNewPlan;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, AdvSpin, Buttons, ExtCtrls, uHBoardSystem;

type
  {$warnings off}
  TfrmNewPlan = class(TForm)
    bbtnOk: TBitBtn;
    bbtnCancel: TBitBtn;
    edtPlanName: TEdit;
    lblMessage: TLabel;
    procedure edtPlanNameChange(Sender: TObject);
  private
    fExistingPlans : TStringList;
    function GetNewPlanName: String;
  public
    Constructor Create(_Owner : TComponent; _ExistingPlans : TStrings); overload;
    Destructor Destroy; override;
    property NewPlanName : String read GetNewPlanName;
  end;
  {$warnings on}

var
  frmNewPlan: TfrmNewPlan;

implementation

{$R *.dfm}

{ TfrmNewPlan }

Constructor TfrmNewPlan.Create(_Owner : TComponent; _ExistingPlans : TStrings);
begin
  inherited Create(Owner);
  fExistingPlans := TStringList.Create();
  fExistingPlans.Assign(_ExistingPlans);
  edtPlanNameChange(nil);
end;

Destructor TfrmNewPlan.Destroy;
begin
  fExistingPlans.free();
  inherited Destroy;
end;

function TfrmNewPlan.GetNewPlanName: String;
begin
  Result := edtPlanName.Text;
end;

procedure TfrmNewPlan.edtPlanNameChange(Sender: TObject);
begin
  bbtnOk.Enabled := (edtPlanName.Text <> '') and (fExistingPlans.IndexOf(edtPlanName.Text) = -1);
  if fExistingPlans.IndexOf(edtPlanName.Text) > -1 then
    lblMessage.Caption := 'Ce nom de plan existe d�j�'
  else
    lblMessage.Caption := '';
end;

end.
