object frmNewPlan: TfrmNewPlan
  Left = 831
  Top = 490
  BorderStyle = bsToolWindow
  Caption = 'Gestion des Plans'
  ClientHeight = 63
  ClientWidth = 434
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    434
    63)
  PixelsPerInch = 96
  TextHeight = 18
  object lblMessage: TLabel
    Left = 4
    Top = 36
    Width = 4
    Height = 18
  end
  object bbtnOk: TBitBtn
    Left = 268
    Top = 34
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Ok'
    Enabled = False
    TabOrder = 1
    Kind = bkOK
  end
  object bbtnCancel: TBitBtn
    Left = 349
    Top = 34
    Width = 82
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '&Annuler'
    TabOrder = 2
    Kind = bkCancel
  end
  object edtPlanName: TEdit
    Left = 4
    Top = 4
    Width = 425
    Height = 26
    TabOrder = 0
    OnChange = edtPlanNameChange
  end
end
