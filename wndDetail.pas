unit wndDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, uHBoardUtils, ImgList;

type

  TfrmDetail = class(TForm)
    bbtnClose: TBitBtn;
    lbStores: TListBox;
    ImageList: TImageList;
    lblArchive: TLabel;
    stArchiveName: TStaticText;
    lblBrands: TLabel;
    lblScreensOrg: TLabel;
    lblUnit: TLabel;
    stBrand: TStaticText;
    stScreensOrg: TStaticText;
    stUnit: TStaticText;
    bbtnResend: TBitBtn;
    procedure lbStoresDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure bbtnResendClick(Sender: TObject);
  private
    fReload : Boolean;
    fStoreParameters : TStoresParameters;
    fParameters : TParameters;
    fArchivesStatus : TStringList;
    fArchiveName: String;
    fUnitDetail: TUnit;
    fUnitPath: String;
    procedure LoadDetails();
    procedure SetArchiveName(const Value: String);
    procedure SetUnitDetail(const Value: TUnit);
  public
    constructor Create(_Owner : TComponent; _Parameters : TParameters; _StoreParameters : TStoresParameters; _UnitDetail : TUnit; _UnitPath, _Archive : String); overload;
    destructor Destroy(); override;
    property UnitDetail : TUnit read FUnitDetail write SetUnitDetail;
    property UnitPath : String read FUnitPath write FUnitPath;
    property ArchivePath : String read FArchiveName write SetArchiveName;
    property Reload : Boolean read fReload;
  end;

var
  frmDetail: TfrmDetail;

implementation

uses wndEditPgm, uCommon;

{$R *.dfm}

constructor TfrmDetail.Create(_Owner : TComponent; _Parameters : TParameters; _StoreParameters : TStoresParameters; _UnitDetail : TUnit; _UnitPath, _Archive : String);
begin
  Inherited Create(_Owner);
  fReload := False;
  fParameters := _Parameters;
  fStoreParameters := _StoreParameters;
  fArchivesStatus := TStringList.Create();
  UnitDetail := _UnitDetail;
  UnitPath := _UnitPath;
  ArchivePath := _Archive;
end;

destructor TfrmDetail.Destroy();
begin
  Inherited Destroy();
  fArchivesStatus.Free();
end;

procedure TfrmDetail.LoadDetails();
var
  nCountStores : Integer;
begin
  lbStores.Clear();
  fArchivesStatus.Clear();
  for nCountStores := 0 to fStoreParameters.StoresDataLocation.Count-1 do
  begin
    lbStores.Items.Add(fStoreParameters.StoresName[nCountStores]);
    fArchivesStatus.Add(
      IntToStr(
        GetArchiveStatus(
          fStoreParameters.StoresDataLocation[nCountStores],
          fArchiveName
          )
        )
      );
  end;
end;

procedure TfrmDetail.lbStoresDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  bmpDisplay :TBitmap;
begin
  bmpDisplay := TBitmap.Create;
  if Index < fArchivesStatus.Count then ImageList.GetBitmap(StrToInt(fArchivesStatus[Index]),bmpDisplay);
  with (Control as TListBox) do
  begin
    Canvas.FillRect(Rect);
    Canvas.TextOut(Rect.Left+ImageList.Height+2,Rect.Top,Items[Index]);
    Canvas.Draw(Rect.Left,Rect.Top,bmpDisplay);
  end;
  bmpDisplay.Free;
end;

procedure TfrmDetail.SetArchiveName(const Value: String);
begin
  FArchiveName := Value;
  stArchiveName.Caption := FArchiveName;
  LoadDetails();
end;

procedure TfrmDetail.SetUnitDetail(const Value: TUnit);
begin
  fUnitDetail := Value;
  stBrand.Caption := FUnitDetail.BrandName;
  stScreensOrg.Caption := FUnitDetail.Screens;
  stUnit.Caption := FUnitDetail.UnitName;
end;

procedure TfrmDetail.bbtnResendClick(Sender: TObject);
begin
  if not (lbStores.ItemIndex = -1) or (MessageDlg('D�sirez vous envoyer toutes les archives ?', mtConfirmation, mbOKCancel, 0) = mrOk) then
    if SendPlans(AddSlash(UnitPath) + fArchiveName, fStoreParameters, lbStores.ItemIndex) then
    begin
      ShowMessage('Envoi effectif !');
      LoadDetails();
      fReload := True;
    end;
end;

end.
