unit wndSelectEntity;

//* 20170911 : Rajout de Log sur les archives
//* 20170913 : Correctif Bug Chemins Absolus
//* 20171002 :  A Faire - Bloquer lors des compressions d�compressions (un click hors de la fen�tre permet de revenir sur les �crans de base)
//*                     - Selection de plusieurs images
//*                     - Rajouter des raccourcis clavier

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, uHBoardUtils, ImgList;

type
  TfrmSelectEntity = class(TForm)
    lblBrands: TLabel;
    cmbBrands: TComboBox;
    lblUnit: TLabel;
    cmbUnits: TComboBox;
    bbtnDetail: TBitBtn;
    bbtnClose: TBitBtn;
    lbArchives: TListBox;
    ImageList: TImageList;
    bbtnEdit: TBitBtn;
    cmbScreens: TComboBox;
    lblScreens: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cmbBrandsClick(Sender: TObject);
    procedure cmbScreensClick(Sender: TObject);
    procedure cmbUnitsClick(Sender: TObject);
    procedure bbtnDetailClick(Sender: TObject);
    procedure lbArchivesDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure btnScreensClick(Sender: TObject);
    procedure bbtnEditClick(Sender: TObject);
    procedure lbArchivesClick(Sender: TObject);
    procedure lbArchivesDblClick(Sender: TObject);
  private
    fArchivesStatus : TStringList;
    fParameters : TParameters;
    fStoreParameters : TStoresParameters;
    procedure LoadBrands;
    procedure LoadScreens;
    procedure LoadUnits;
    procedure LoadArchives(_UnitPath: String);
    function CurrentServerPath: String;
    function CurrentBrandPath: String;
    function CurrentScreensOrgPath : String;
    function CurrentUnit: TUnit;
    function CurrentUnitPath: String;
    procedure ControlButtonsState;
  public
  end;

var
  frmSelectEntity: TfrmSelectEntity;

implementation

{$R *.dfm}

uses uCommon, wndSelectPgm, uHBoardSystem, wndEditMonitors, wndDetail,
  wndEditPgm;

procedure TfrmSelectEntity.FormCreate(Sender: TObject);
begin
  fParameters := TParameters.Create(ExtractFilePath(Application.Exename));
  fStoreParameters := TStoresParameters.Create();
  fArchivesStatus := TStringList.Create();
  LoadBrands;
end;

procedure TfrmSelectEntity.FormDestroy(Sender: TObject);
begin
  fStoreParameters.Free();
  fArchivesStatus.Free();
end;

procedure TfrmSelectEntity.LoadBrands();
begin
  LoadFolders(CurrentServerPath, cmbBrands.Items);
  SelectItem(cmbBrands);
end;

procedure TfrmSelectEntity.LoadScreens();
begin
  LoadFolders(CurrentBrandPath, cmbScreens.Items);
  SelectItem(cmbScreens);
end;

procedure TfrmSelectEntity.LoadUnits();
begin
  LoadFolders(CurrentScreensOrgPath, cmbUnits.Items);
  SelectItem(cmbUnits);
  ControlButtonsState();
end;

procedure TfrmSelectEntity.cmbBrandsClick(Sender: TObject);
begin
  LoadScreens;
end;

procedure TfrmSelectEntity.cmbScreensClick(Sender: TObject);
begin
  LoadUnits;
end;

procedure TfrmSelectEntity.cmbUnitsClick(Sender: TObject);
begin
  bbtnEdit.Enabled := ContainsElements(cmbUnits);
  fStoreParameters.Path := CurrentUnitPath;
  LoadArchives(CurrentUnitPath);
end;

procedure TfrmSelectEntity.bbtnDetailClick(Sender: TObject);
begin
  frmDetail := TfrmDetail.Create( Self,
                                  fParameters,
                                  fStoreParameters,
                                  CurrentUnit,
                                  CurrentUnitPath,
                                  ActualValue(lbArchives)
                                );
  frmDetail.ShowModal();
  if frmDetail.Reload then LoadArchives(CurrentUnitPath);
  frmDetail.Free();
end;

procedure TfrmSelectEntity.LoadArchives(_UnitPath: String);
var
  nStatus,
  nCurrentStatus,
  nCountFiles,
  nCountStores : Integer;
begin
  LoadFiles(_UnitPath, lbArchives.Items, '*.zip');
  fArchivesStatus.Clear();
  for nCountFiles := 0 to lbArchives.Count-1 do
  begin
    nStatus := CST_OK;
    for nCountStores := 0 to fStoreParameters.StoresDataLocation.Count-1 do
    begin
      nCurrentStatus := GetArchiveStatus(fStoreParameters.StoresDataLocation[nCountStores], lbArchives.Items[nCountFiles]);
      if (nCurrentStatus = CST_SND) and (nStatus = CST_OK) then nStatus := CST_SND
      else if (nCurrentStatus = CST_KO) then nStatus := CST_KO;
    end;
    fArchivesStatus.Add(IntToStr(nStatus));
  end;
end;

procedure TfrmSelectEntity.lbArchivesDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  bmpDisplay :TBitmap;
begin
  bmpDisplay := TBitmap.Create;
  if Index < fArchivesStatus.Count then ImageList.GetBitmap(StrToInt(fArchivesStatus[Index]),bmpDisplay);
  with (Control as TListBox) do
  begin
    Canvas.FillRect(Rect);
    Canvas.TextOut(Rect.Left+ImageList.Height+2,Rect.Top,Items[Index]);
    Canvas.Draw(Rect.Left+1,Rect.Top+1,bmpDisplay);
  end;
  bmpDisplay.Free;
end;

function TfrmSelectEntity.CurrentServerPath: String;
begin
  result := AddSlash(fParameters.ServerFilesLocation);
end;

function TfrmSelectEntity.CurrentBrandPath: String;
begin
  result := CurrentServerPath + AddSlash(ActualValue(cmbBrands));
end;

function TfrmSelectEntity.CurrentScreensOrgPath: String;
begin
  result := CurrentBrandPath + AddSlash(ActualValue(cmbScreens));
end;

function TfrmSelectEntity.CurrentUnitPath: String;
begin
  result := CurrentScreensOrgPath + AddSlash(ActualValue(cmbUnits));
end;

function TfrmSelectEntity.CurrentUnit: TUnit;
begin
  Result.BrandName := ActualValue(cmbUnits);
  Result.Screens := ActualValue(cmbScreens);
  Result.UnitName := ActualValue(cmbUnits);
end;

procedure TfrmSelectEntity.btnScreensClick(Sender: TObject);
var
  frmEditMonitors : TfrmEditMonitors;
begin
  frmEditMonitors := TfrmEditMonitors.Create(Self, fParameters, CurrentBrandPath, CurrentScreensOrgPath);
  frmEditMonitors.ShowModal();
  frmEditMonitors.Free();
end;

procedure TfrmSelectEntity.bbtnEditClick(Sender: TObject);
begin
  frmEditPgm := TfrmEditPgm.Create( Self,
                                    fParameters,
                                    CurrentUnit,
                                    CurrentUnitPath,
                                    ActualValue(lbArchives)
                                   );
  frmEditPgm.ShowModal();
  frmEditPgm.Free();
  LoadArchives(CurrentUnitPath);
end;

procedure TfrmSelectEntity.lbArchivesClick(Sender: TObject);
begin
  ControlButtonsState();
end;

procedure TfrmSelectEntity.ControlButtonsState();
begin
  bbtnEdit.Enabled := lbArchives.ItemIndex > -1;
  bbtnDetail.Enabled := lbArchives.ItemIndex > -1;
end;

procedure TfrmSelectEntity.lbArchivesDblClick(Sender: TObject);
begin
  bbtnDetailClick(Sender);
end;

end.
