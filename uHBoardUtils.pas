unit uHBoardUtils;

interface

uses Classes, StdCtrls, Graphics, Types;

Const

  CST_NONE = -1;
  CST_OK = 0;
  CST_SND = 1;
  CST_KO = 2;

  clLightOrange = TColor($30CDFC);
  clOrange = TColor($00D8FF);
  LocalColors: array[0..17] of TIdentMapEntry = (
    (Value: clWhite; Name: 'clWhite'),
    (Value: clBlack; Name: 'clBlack'),
    (Value: clAqua; Name: 'clAqua'),
    (Value: clBlue; Name: 'clBlue'),
    (Value: clFuchsia; Name: 'clFuchsia'),
    (Value: clGray; Name: 'clGray'),
    (Value: clGreen; Name: 'clGreen'),
    (Value: clLightOrange; Name: 'clLightOrange'),
    (Value: clLime; Name: 'clLime'),
    (Value: clMaroon; Name: 'clMaroon'),
    (Value: clNavy; Name: 'clNavy'),
    (Value: clOlive; Name: 'clOlive'),
    (Value: clOrange; Name: 'clOrange'),
    (Value: clPurple; Name: 'clPurple'),
    (Value: clRed; Name: 'clRed'),
    (Value: clSilver; Name: 'clSilver'),
    (Value: clTeal; Name: 'clTeal'),
    (Value: clYellow; Name: 'clYellow')
    );

  CST_DAYS : array[0..6] of string = ('Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim');

  CST_MAX_IMAGES_PER_PAGE = 6;

  CST_MONITOR_CONFIGURATION_FILENAME = 'monitor.csv';

  CST_INI_LOCATIONS_SECTION = 'Locations';

  CST_NO_ELEMENT = '*** Aucun �l�ment trouv� ***';

// Zone li�e aux moniteurs //
  CST_DFT_TEMP_FOLDER = 'wrk';
  CST_IMAGES_FOLDER = 'imgs';
  CST_LOCATION_FILENAME = 'location.txt';

  CST_BMP_EXT = '*.bmp';
  CST_JPG_EXT = '*.jpg';
  CST_JPEG_EXT = '*.jpeg';
  CST_PNG_EXT = '*.png';

  CST_INI_CONFIGURATION_SECTION = 'Config';
  CST_INI_SCREEN_PREFIX = 'Ecran';
  CST_INI_ROTATE_DELAY = 'Rotat';
  CST_MAX_SCREENS = 10;
  CST_DEFAULT_DELAY = 10;
// Fin de zone li�e aux moniteurs //

Type
  TParameters = Class
  private
    fServerFilesLocation : String;
    fLastPicturesLocation : String;
    fBackupLocation : String;
    fTemporaryFolder : String;
    fFileName : String;
    class function CST_INI_CONFIGURATION_FILENAME : String;
    class function CST_INI_CONFIGURATION_SECTION : String;
    class function CST_INI_PICT_FOLDER : String;
    class function CST_INI_TEMP_FOLDER : String;
    class function CST_INI_UNIT_FOLDER : String;
    class function CST_INI_BACKUP_FOLDER : String;
    function GetBackupLocation: String;
    function GetLastPicturesLocation: String;
    function GetServerFilesLocation: String;
    function GetTemporaryFolder: String;
  public
    constructor Create(_FilePath : String); overload;
  published
    Procedure Load(_FileName : String);
    Procedure Save();
    property ServerFilesLocation : String read GetServerFilesLocation write fServerFilesLocation;
    property LastPicturesLocation : String read GetLastPicturesLocation write fLastPicturesLocation;
    property BackupLocation : String read GetBackupLocation write fBackupLocation;
    property TemporaryFolder : String read GetTemporaryFolder write fTemporaryFolder;
    property FileName : String read fFileName;
  end;

  TStoresParameters = Class
  private
    FStoresDataLocation: TStringList;
    FStoresName: TStringList;
    FScreens: TStringList;
    FFileName : String;
    fPath: String;
    class function CST_INI_CONFIGURATION_FILENAME : String;
    class function CST_INI_SCREENS_SECTION : String;
    class function CST_INI_SCREEN_PREFIX : String;
    class function CST_INI_STORES_SECTION : String;
    class function CST_MAX_SCREENS : Integer;
    class function CST_MAX_STORES : Integer;
    procedure SetScreens(const Value: TStringList);
    procedure SetStoresDataLocation(const Value: TStringList);
    procedure SetStoresName(const Value: TStringList);
    procedure SetPathName(const _PathName: String);
  public
    constructor Create(_FilePath : String = ''); overload;
  published
    Procedure Load(_FileName : String);
    Procedure Save(_FileName : String);
    Property Screens : TStringList read FScreens;
    Property StoresName : TStringList read FStoresName;
    Property StoresDataLocation : TStringList read FStoresDataLocation;
    Property FileName : String read FFileName;
    Property Path : String read fPath write SetPathName;
  end;

  TLocalConfig = Class
  private
    FFileName : String;
    FRotationDelay : Integer;
    class function CST_INI_CONFIGURATION_FILENAME : String;
    class function CST_INI_CONFIGURATION_SECTION : String;
    class function CST_INI_ROTATION : String;
    class function CST_DEFAULT_ROTATION_DELAY: Integer;
    procedure SetRotationDelay(const Value: Integer);
  public
    constructor Create(_FilePath : String); overload;
  published
    Procedure Load(_FileName : String);
    Procedure Save(_FileName : String);
    Property RotationDelay : Integer read FRotationDelay write SetRotationDelay;
    Property FileName : String read FFileName;
  end;

type
  TUnit = record
    BrandName : String;
    Screens : String;
    UnitName : String;
  end;

  function ContainsElements(_Results : TComboBox) : boolean;

  procedure SelectItem(_Results : TListBox; var _NewIndex : Integer); overload;
  procedure SelectItem(_Results : TListBox); overload;
  procedure SelectItem(_Results : TComboBox; var _NewIndex : Integer); overload;
  procedure SelectItem(_Results : TComboBox); overload;

  procedure LoadFolders(_Chemin : String; _Results: TStrings; _Filter : String = '*.*');
  procedure LoadFiles(_Chemin : String; _Results: TStrings; _Filter : String = '*.*');

  function AddRect(_Rect : TRect; _AddValue : Integer) : TRect;
  function StringToLocalColor(const S: string): TColor;

  function GetArchiveStatus(_StoreLocation, _ArchiveName : String) : Integer;
  function SendPlans(_ArchiveName : String; _StoreParameters : TStoresParameters; _Index : Integer = -1) : Boolean;

implementation

uses SysUtils, uCommon, IniFiles, Forms, StrUtils, Dialogs, Controls;

function AddRect(_Rect : TRect; _AddValue : Integer) : TRect;
begin
  Result := Rect(_Rect.Left+_AddValue, _Rect.Top+_AddValue, _Rect.Right-_AddValue, _Rect.Bottom-_AddValue);
end;

function StringToLocalColor(const S: string): TColor;
  function LocalIdentToColor(const Ident: string; var Color: Longint): Boolean;
  begin
    Result := IdentToInt(Ident, Color, LocalColors);
  end;
begin
  if not LocalIdentToColor(S, Longint(Result)) then
    Result := TColor(StrToInt(S));
end;

procedure LoadFolders(_Chemin : String; _Results: TStrings; _Filter : String = '*.*');
var
  srResult : TSearchRec;
begin
  _Results.Clear;
  if FindFirst(AddSlash(_Chemin)+_Filter, faDirectory, srResult) = 0 then
  repeat
    if ((srResult.attr and faDirectory) = faDirectory) and not (srResult.Name[1] = '.')
      then _Results.Add(srResult.Name);
  until FindNext(srResult) <> 0;
  FindClose(srResult);
end;

procedure LoadFiles(_Chemin : String; _Results: TStrings; _Filter : String = '*.*');
var
  srResult : TSearchRec;
begin
  _Results.Clear;
  if FindFirst(AddSlash(_Chemin)+_Filter, faAnyFile, srResult) = 0 then
  repeat
    if not ((srResult.attr and faDirectory) = faDirectory) and not (srResult.Name[1] = '.')
      then _Results.Add(srResult.Name);
  until FindNext(srResult) <> 0;
  FindClose(srResult);
end;

function ContainsElements(_Results : TComboBox) : boolean;
begin
  Result := (_Results.Items.Count >= 1) and not (_Results.Items[0] = CST_NO_ELEMENT);
end;

procedure SelectItem(_Results : TListBox;var _NewIndex : Integer); overload;
begin
  _Results.Enabled := _Results.Items.Count <> 0;
  if _Results.Items.Count = 0 then _Results.Items.Add(CST_NO_ELEMENT);
  if not (_NewIndex = -1) then
  begin
    _Results.ItemIndex := _NewIndex;
    _NewIndex := -1;
  end
  else
    _Results.ItemIndex := 0;
  if Assigned(_Results.OnClick) then _Results.OnClick(nil);
end;

procedure SelectItem(_Results : TListBox); overload;
var
  nVoidValue : Integer;
begin
  nVoidValue := -1;
  SelectItem(_Results, nVoidValue);
end;

procedure SelectItem(_Results : TComboBox;var _NewIndex : Integer); overload;
begin
  _Results.Enabled := _Results.Items.Count <> 0;
  if _Results.Items.Count = 0 then _Results.Items.Add(CST_NO_ELEMENT);
  if not (_NewIndex = -1) then
  begin
    _Results.ItemIndex := _NewIndex;
    _NewIndex := -1;
  end
  else
    _Results.ItemIndex := 0;
  if Assigned(_Results.OnClick) then _Results.OnClick(nil);
end;

procedure SelectItem(_Results : TComboBox); overload;
var
  nVoidValue : Integer;
begin
  nVoidValue := -1;
  SelectItem(_Results, nVoidValue);
end;

{ TParameters }

constructor TParameters.Create(_FilePath : String);
begin
  inherited Create();
  FFileName := AddSlash(_FilePath) + CST_INI_CONFIGURATION_FILENAME;
  Load(FFileName);
end;

class function TParameters.CST_INI_BACKUP_FOLDER: String;
begin
  Result := 'RepertoireSauvegarde';
end;

class function TParameters.CST_INI_CONFIGURATION_FILENAME : String;
begin
  Result := 'hBoardCentral.ini';
end;

class function TParameters.CST_INI_CONFIGURATION_SECTION: String;
begin
  Result := 'Config';
end;

class function TParameters.CST_INI_PICT_FOLDER: String;
begin
  Result := 'DernierRepertoireDesImages';
end;

class function TParameters.CST_INI_TEMP_FOLDER: String;
begin
  Result := 'RepertoireTemporaire';
end;

class function TParameters.CST_INI_UNIT_FOLDER: String;
begin
  Result := 'RepertoireUnites';
end;

function TParameters.GetBackupLocation: String;
begin
  Result := AddSlash(fBackupLocation);
end;

function TParameters.GetLastPicturesLocation: String;
begin
  Result := AddSlash(fLastPicturesLocation);
end;

function TParameters.GetServerFilesLocation: String;
begin
  Result := AddSlash(fServerFilesLocation);
end;

function TParameters.GetTemporaryFolder: String;
begin
  Result := AddSlash(fTemporaryFolder);
end;

procedure TParameters.Load(_FileName : String);
var
  iniFile : TInifile;
  procedure CreateDefaultConfiguration();
  begin
    // A Faire FFileName //
  end;
begin
  if not FileExists(_FileName) then CreateDefaultConfiguration();
  iniFile := TIniFile.Create(FFileName);
  fServerFilesLocation := RelToAbs(iniFile.ReadString(CST_INI_CONFIGURATION_SECTION, CST_INI_UNIT_FOLDER, ExtractFilePath(Application.Exename)), ExtractFilePath(Application.Exename));
  fLastPicturesLocation := RelToAbs(iniFile.ReadString(CST_INI_CONFIGURATION_SECTION, CST_INI_PICT_FOLDER, ExtractFilePath(Application.Exename)), ExtractFilePath(Application.Exename));
  if fLastPicturesLocation = '' then fLastPicturesLocation := ExtractFilePath(Application.Exename);
  fBackupLocation := RelToAbs(iniFile.ReadString(CST_INI_CONFIGURATION_SECTION, CST_INI_BACKUP_FOLDER, ExtractFilePath(Application.Exename)), ExtractFilePath(Application.Exename));
  fTemporaryFolder := RelToAbs(iniFile.ReadString(CST_INI_CONFIGURATION_SECTION, CST_INI_TEMP_FOLDER, ''), ExtractFilePath(Application.Exename));
  iniFile.Free();
end;

procedure TParameters.Save();
var
  iniFile : TInifile;
begin
  iniFile := TIniFile.Create(FFileName);
  iniFile.WriteString(CST_INI_CONFIGURATION_SECTION, CST_INI_PICT_FOLDER, fLastPicturesLocation);
  iniFile.WriteString(CST_INI_CONFIGURATION_SECTION, CST_INI_UNIT_FOLDER, fServerFilesLocation);
  iniFile.WriteString(CST_INI_CONFIGURATION_SECTION, CST_INI_PICT_FOLDER, fLastPicturesLocation);
  iniFile.WriteString(CST_INI_CONFIGURATION_SECTION, CST_INI_BACKUP_FOLDER, fBackupLocation);
  iniFile.WriteString(CST_INI_CONFIGURATION_SECTION, CST_INI_TEMP_FOLDER, fTemporaryFolder);
  iniFile.Free();
end;

{ TLocalConfig }

constructor TLocalConfig.Create(_FilePath : String);
begin
  inherited Create();
  Load(AddSlash(_FilePath) + CST_INI_CONFIGURATION_FILENAME);
end;

class function TLocalConfig.CST_INI_CONFIGURATION_FILENAME: String;
begin
  Result := 'Config.ini';
end;

class function TLocalConfig.CST_INI_CONFIGURATION_SECTION: String;
begin
  Result := 'Config';
end;

class function TLocalConfig.CST_DEFAULT_ROTATION_DELAY: Integer;
begin
  Result := 30;
end;

class function TLocalConfig.CST_INI_ROTATION: String;
begin
  Result := 'Rotation';
end;

procedure TLocalConfig.Load(_FileName: String);
var
  iniFile : TIniFile;
begin
  iniFile := TIniFile.Create(_FileName);
  FFileName := _FileName;
  RotationDelay := iniFile.ReadInteger(CST_INI_CONFIGURATION_SECTION, CST_INI_ROTATION, CST_DEFAULT_ROTATION_DELAY);
  iniFile.Free();
end;

procedure TLocalConfig.Save;
var
  iniFile : TIniFile;
begin
  iniFile := TIniFile.Create(FFileName);
  iniFile.WriteInteger(CST_INI_CONFIGURATION_SECTION, CST_INI_ROTATION, RotationDelay);
  iniFile.Free();
end;

procedure TLocalConfig.SetRotationDelay(const Value: Integer);
begin
  FRotationDelay := Value;
end;

{ TStoreParameters }

constructor TStoresParameters.Create(_FilePath: String = '');
begin
  inherited Create();
  FScreens := TStringList.Create;
  FStoresName := TStringList.Create;
  FStoresDataLocation := TStringList.Create;
  Path := _FilePath;
end;

class function TStoresParameters.CST_INI_CONFIGURATION_FILENAME: String;
begin
  Result := 'Magasins.ini';
end;

class function TStoresParameters.CST_INI_SCREEN_PREFIX: String;
begin
  Result := 'Ecran';
end;

class function TStoresParameters.CST_INI_SCREENS_SECTION: String;
begin
  Result := 'Ecrans';
end;

class function TStoresParameters.CST_INI_STORES_SECTION: String;
begin
  Result := 'Magasins';
end;

class function TStoresParameters.CST_MAX_SCREENS: Integer;
begin
  Result := 99;
end;

class function TStoresParameters.CST_MAX_STORES: Integer;
begin
  Result := 9999;
end;

procedure TStoresParameters.Load(_FileName: String);
var
  iniFile : TIniFile;
  procedure LoadScreens;
  var
    nCount : Integer;
    sScreenName : String;
  const
    CST_NONE_VALUE = '*NONE*';
  begin
    FScreens.Clear();
    nCount := 1;
    repeat
      sScreenName := iniFile.ReadString(CST_INI_SCREENS_SECTION,  CST_INI_SCREEN_PREFIX + Pad(IntToStr(nCount), 2, '0', True), CST_NONE_VALUE);
      if not (sScreenName = CST_NONE_VALUE) then FScreens.Add(Pad(IntToStr(nCount), 2, '0', True) + ' ' + sScreenName);
      nCount := nCount + 1;
    until (sScreenName = CST_NONE_VALUE) and (nCount <= CST_MAX_SCREENS);
  end;
  procedure LoadStores;
  var
    nCount : Integer;
    sReadValue,
    sStoreName,
    sStoreLocation : String;
  const
    CST_NONE_VALUE = '*NONE*';
  begin
    FStoresName.Clear();
    FStoresDataLocation.Clear();
    nCount := 1;
    repeat
      sReadValue := iniFile.ReadString(CST_INI_STORES_SECTION, Pad(IntToStr(nCount), 4, '0', True), CST_NONE_VALUE);
      try
        sStoreName := Copy(sReadValue, 1, Pos(',', sReadValue)-1);
        sStoreLocation := RightStr(sReadValue, Length(sReadValue) - Pos(',', sReadValue));
        if not (sStoreName = '') and not (sStoreLocation = '') then
        begin
          FStoresName.Add(sStoreName);
          FStoresDataLocation.Add(AddSlash(sStoreLocation));
        end;
      except
      end;
      nCount := nCount + 1;
    until (sReadValue = CST_NONE_VALUE) and (nCount <= CST_MAX_STORES);
  end;
begin
  iniFile := TIniFile.Create(_FileName);
  FFileName := _FileName;
  LoadScreens();
  LoadStores();
  iniFile.Free();
end;

procedure TStoresParameters.Save(_FileName: String);
begin

end;

procedure TStoresParameters.SetScreens(const Value: TStringList);
begin

end;

procedure TStoresParameters.SetStoresDataLocation(const Value: TStringList);
begin

end;

procedure TStoresParameters.SetStoresName(const Value: TStringList);
begin

end;

function GetArchiveStatus(_StoreLocation, _ArchiveName : String) : Integer;
begin
  if FileExists(AddSlash(_StoreLocation) + _ArchiveName) then
    Result := CST_SND
  else if FileExists(AddSlash(_StoreLocation) + ExtractOnlyFileName(_ArchiveName) + '.sav') then
    Result := CST_OK
  else
    Result := CST_KO;
end;

function SendPlans(_ArchiveName : String; _StoreParameters : TStoresParameters; _Index : Integer = -1) : Boolean;
var
  nCountStores : Integer;
  function SendOnePlan(StoreDataLocation : String) : Boolean;
  var
    sSourceFileName,
    sDestFileName : String;
  begin
    Result := True;
    sSourceFileName := _ArchiveName;
    sDestFileName := AddSlash(StoreDataLocation) + ExtractFileName(_ArchiveName);
    if not DirectoryExists(AddSlash(StoreDataLocation)) then
      CreateDir(AddSlash(StoreDataLocation));
    try
      CreateEmptyFile(AddSlash(StoreDataLocation) + 'LOCK.TXT');
    except
    end;
    if not PascalCopyfile(sSourceFileName, sDestFileName, False) then
      Result := (MessageDlg('Le fichier ' + sSourceFileName + ' n''a pas pu �tre copi� vers ' + sDestFileName + ', voulez-vous continuer ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes);
    try
      if FileExists(AddSlash(StoreDataLocation) + 'LOCK.TXT') then DeleteFile(AddSlash(StoreDataLocation) + 'LOCK.TXT');
    except
    end;
  end;
begin
  result := False;
  if not (_Index = -1) then
    SendOnePlan(_StoreParameters.StoresDataLocation[_Index])
  else
    for nCountStores := 0 to _StoreParameters.StoresDataLocation.Count-1 do
      if not SendOnePlan(_StoreParameters.StoresDataLocation[nCountStores]) then Exit;
  Result := True;
end;

procedure TStoresParameters.SetPathName(const _PathName: String);
begin
  if not (_PathName = '') and not (_PathName = fPath) then
  begin
    fPath := _PathName;
    Load(AddSlash(fPath) + CST_INI_CONFIGURATION_FILENAME);
  end;
end;

end.

