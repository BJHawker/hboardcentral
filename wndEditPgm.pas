unit wndEditPgm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, BaseGrid, AdvGrid, Buttons, uHBoardSystem,
  ExtCtrls, Menus, Mask, AdvSpin, uHBoardUtils, ComCtrls, ImgList;

type
  TCellInfo = record
    BkColor: TColor;
    Plan : String;
  end;

  {*
  20161204 : A Faire
     - Sauvegarder Config.ini dans le r�pertoire d'origine pour qu'il devienne le d�faut
     - Definir DEBUG pour les messages de suppression
  *}

type

  TfrmEditPgm = class(TForm)
    gbUnits: TGroupBox;
    lblProgrammation: TLabel;
    lblPlans: TLabel;
    lbPlans: TListBox;
    gbMonitors: TGroupBox;
    lblMediasCount: TLabel;
    pnlImage: TPanel;
    Panel1: TPanel;
    Image1: TImage;
    sbDeleteImage1: TSpeedButton;
    Panel2: TPanel;
    Image2: TImage;
    sbDeleteImage2: TSpeedButton;
    Panel3: TPanel;
    Image3: TImage;
    sbDeleteImage3: TSpeedButton;
    Panel4: TPanel;
    Image4: TImage;
    sbDeleteImage4: TSpeedButton;
    Panel5: TPanel;
    Image5: TImage;
    sbDeleteImage5: TSpeedButton;
    Panel6: TPanel;
    Image6: TImage;
    sbDeleteImage6: TSpeedButton;
    bbtnPrevious: TBitBtn;
    bbtnNext: TBitBtn;
    bbtnAddImage: TBitBtn;
    odFichiers: TOpenDialog;
    lblTips: TLabel;
    sgSchedule: TStringGrid;
    pmSchedule: TPopupMenu;
    bbtnSave: TBitBtn;
    bbtnLoadMultipleFiles: TBitBtn;
    pmPlans: TPopupMenu;
    mnuAddPlan: TMenuItem;
    mnuDeleteCurrentPlan: TMenuItem;
    pmImages: TPopupMenu;
    mnuDeleteCurrentImage: TMenuItem;
    mnuAddImage: TMenuItem;
    seDelay: TAdvSpinEdit;
    lblDelay: TLabel;
    bbtnClose: TBitBtn;
    dtDate: TDateTimePicker;
    lblActivateTime: TLabel;
    ImageList: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbPlansClick(Sender: TObject);

// Zone li�e aux moniteurs //
    procedure MonitorClick(Sender: TObject);
    procedure bbtnAddImageClick(Sender: TObject);
    procedure OnDeleteImageClick(Sender: TObject);
    procedure ImagesClick(Sender: TObject);
    procedure bbtnPreviousClick(Sender: TObject);
    procedure bbtnNextClick(Sender: TObject);
    procedure btnLoadPicturesClick(Sender: TObject);
// Fin de zone li�e aux moniteurs //

//*  Zone de Gestion de la Programmation ******
    procedure sgScheduleDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure bbtnSaveClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure bbtnLoadMultipleFilesClick(Sender: TObject);
    procedure bbtnEditUnitClick(Sender: TObject);
    procedure mnuAddPlanClick(Sender: TObject);
    procedure mnuDeleteCurrentPlanClick(Sender: TObject);
    procedure mnuDeleteCurrentImageClick(Sender: TObject);
    procedure mnuAddImageClick(Sender: TObject);
    procedure seDelayChange(Sender: TObject);
    procedure lbPlansDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
//*  Fin de Zone de Gestion de la Programmation ******

  private

    nLastPlanSelected : Integer;

    lstImages,
    lstDeleteButtons : TList;
    fPage: Integer;
    fCurrentMonitor : Integer;
    fPlans : TPlans;

    fLocalConfig : TLocalConfig;
    fStoreParameters : TStoresParameters;
    fParameters : TParameters;

    aCellsInfo: array of TCellInfo;

// Zone li�e aux moniteurs //
    fEditMode: Boolean;
    fUnitArchive: String;
    fUnitDetail: TUnit;
    fUnitPath: String;

    procedure ClearWorkingFolder();
    procedure SelectAndCopyOneFile(_Image : TImage; _NewImage : Boolean = False);
    procedure SelectAndCopyMultipleFiles;
    procedure CheckNavigation;
    procedure InitImages();
    function IsImage(_FileName : String) : boolean;
    procedure DisplayImages();
    procedure DeleteImage(_ImageIndex: Integer);
    procedure DisplayImage(_Image : TImage; _ImageFile : String);
    procedure ClearImage(_Image : TImage);
    procedure SetPage(const Value: Integer);
    function MaxPages: Integer;
    function DisplayMonitors(_Path : String) : Boolean;
    function GetCurrentMonitor() : TMonitor;

// Fin de zone li�e aux moniteurs //

//*  Zone de Gestion de la Programmation ******
    procedure AppShowHint(var HintStr: String; var CanShow: Boolean; var HintInfo: THintInfo);
    Procedure PreparePopup();
    procedure PrepareGrid();
    procedure LoadCellsInfos();
    function CellCoord(_Row, _Col: Integer) : Integer;
    procedure MenuClick(Sender: TObject);
    procedure mnuNewPlanClick(Sender: TObject);
    function GetMenuCellInfo(_MenuItem: TMenuItem): TCellInfo;

    procedure AffectPlan(_BkColor : TColor; _PlanName : String);
    function AddPlan(_AffectNewPlan : Boolean) : TPlan;
    procedure DeleteCurrentPlan;

//*  Fin de Zone de Gestion de la Programmation ******

    function GetCurrentPlan: TPlan;
    procedure SaveLastLocation();

    procedure Save;
    procedure SavePlans;
    procedure SetEditMode(const Value: Boolean);
    procedure RefreshMonitors;

    function GetCurrentDelay: Integer;
    procedure SetCurrentDelay(const Value: Integer);

    function GetRootFolder: String;
    function GetArchiveName: String;

    procedure SetUnitArchive(const Value: String);
    procedure SetUnitDetail(const Value: TUnit);
    procedure SetUnitPath(const Value: String);
    procedure PrepareWorkingFolder;
    function GetNewPlanColorIndex() : Integer;
    procedure LoadPlans(_ForcePlanToSelect : String = '');

  public

    constructor Create(_Owner: TComponent; _Parameters: TParameters; _UnitDetail : TUnit; _UnitPath, _UnitArchive : String); overload;

    procedure DisplayPlans();
    procedure DisplayScheduler(_Scheduler : TScheduler);

    property CurrentPlan : TPlan read GetCurrentPlan;
    property CurrentMonitor : TMonitor read GetCurrentMonitor;
    property CurrentDelay : Integer read GetCurrentDelay write SetCurrentDelay;
    property Page : Integer read fPage write SetPage;

    property EditMode : Boolean read fEditMode write SetEditMode;

    property UnitDetail : TUnit read fUnitDetail write SetUnitDetail;
    property UnitPath : String read fUnitPath write SetUnitPath;
    property UnitArchive : String read fUnitArchive write SetUnitArchive;

    property RootFolder : String read GetRootFolder;

    property ArchiveName : String read GetArchiveName; 

  end;

var
  frmEditPgm: TfrmEditPgm;

implementation

uses uCommon, iniFiles, GraphicEx, wndWait, wndZipAndUnzip, DateUtils, Math,
  wndNewPlan;

{$R *.dfm}

//*  Zone de Gestion de la Programmation ******

function TfrmEditPgm.CellCoord(_Row, _Col : Integer) : Integer;
begin
  Result := (_Row * sgSchedule.ColCount) + _Col;
end;

procedure TfrmEditPgm.AppShowHint(var HintStr: String; var CanShow: Boolean; var HintInfo: THintInfo);
var
  nRow, nCol: Integer;
begin
  if HintInfo.HintControl = sgSchedule then
  begin
    nRow := 0;
    nCol := 0;
    sgSchedule.MouseToCell(HintInfo.CursorPos.X, HintInfo.CursorPos.Y, nCol, nRow);
    HintStr := aCellsInfo[CellCoord(nRow, nCol)].Plan;
    if HintStr <> '' then
      HintInfo.CursorRect := sgSchedule.CellRect(nCol, nRow);
  end;
end;

procedure TfrmEditPgm.sgScheduleDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  sCellText: string;
  nSavedAlign: word;
begin
  sCellText := sgSchedule.Cells[ACol, ARow];
  if gdFixed in State then
  begin
    sgSchedule.Canvas.Brush.Color := sgSchedule.FixedColor;
    sgSchedule.Canvas.FillRect(Rect);
  end
  else if (State * [gdSelected]) <> [] then
  begin
    sgSchedule.Canvas.Brush.Color := clHighlight;
    sgSchedule.Canvas.FillRect(Rect);
    if aCellsInfo[CellCoord(ARow, ACol)].BkColor <> sgSchedule.Color then
    begin
      sgSchedule.Canvas.Brush.Color := aCellsInfo[CellCoord(ARow, ACol)].BkColor;
      sgSchedule.Canvas.FillRect(AddRect(Rect, 5));
    end;
  end
  else
  begin
    sgSchedule.Canvas.Brush.Color := aCellsInfo[CellCoord(ARow, ACol)].BkColor;
    sgSchedule.Canvas.FillRect(Rect);
  end;

  if gdFixed in State then
    Frame3D(sgSchedule.Canvas, Rect, clHighlight, clBtnShadow, 1);

  if gdFocused in State then
    sgSchedule.Canvas.DrawFocusRect(Rect);

  if sCellText <> '' then
  begin
    nSavedAlign := SetTextAlign(sgSchedule.Canvas.Handle, TA_CENTER);
    sgSchedule.Canvas.TextRect(Rect, Rect.Left + (Rect.Right - Rect.Left) div 2, Rect.Top + 2, sCellText);
    SetTextAlign(sgSchedule.Canvas.Handle, nSavedAlign);
  end;
end;

procedure TfrmEditPgm.PreparePopup();
var
  nCount : Integer;
  procedure AddMenu(_Caption : String; _ImageIndex : Integer; _Event : TNotifyEvent);
  var
    miNewMenu : TMenuItem;
  begin
    miNewMenu := TMenuItem.Create(pmSchedule);
    pmSchedule.Items.Add(miNewMenu);
    miNewMenu.Caption := _Caption;
    miNewMenu.Tag := _ImageIndex;
    miNewMenu.OnClick := _Event;
    ImageList.GetBitmap(_ImageIndex, miNewMenu.Bitmap);
  end;
begin
  pmSchedule.Items.Clear;
  for nCount := 0 to fPlans.Count-1 do AddMenu(fPlans[nCount].Name, fPlans[nCount].Tag, MenuClick);
  AddMenu('&Nouveau Plan', 0, mnuNewPlanClick);
end;

procedure TfrmEditPgm.PrepareGrid();
var
  nCount : Integer;
begin
  for nCount := 1 to sgSchedule.ColCount-1 do
    sgSchedule.Cells[nCount, 0] := Pad(IntToStr(nCount-1)+'h', 3, '0', True);
  for nCount := 1 to sgSchedule.RowCount-1 do
    sgSchedule.Cells[0, nCount] := CST_DAYS[nCount-1];
end;

procedure TfrmEditPgm.LoadCellsInfos();
var
  nCount : Integer;
begin
  SetLength(aCellsInfo, sgSchedule.RowCount * sgSchedule.ColCount);
  for nCount := Low(aCellsInfo) to High(aCellsInfo) do
  begin
    aCellsInfo[nCount].BkColor := sgSchedule.Color;
    aCellsInfo[nCount].Plan := '';
  end;
end;

procedure TfrmEditPgm.MenuClick(Sender: TObject);
begin
  AffectPlan(LocalColors[TMenuItem(Sender).Tag].Value, ReplaceString(TMenuItem(Sender).Caption, '&', ''));
end;

procedure TfrmEditPgm.AffectPlan(_BkColor : TColor; _PlanName : String);
var
  grSelection : TGridRect;
  nRow, nCol : Integer;
  ciNewPlan : TCellInfo;
begin
  EditMode := True;
  grSelection := sgSchedule.Selection;
  ciNewPlan.BkColor := _BkColor;
  ciNewPlan.Plan := _PlanName;
  for nRow := grSelection.Top to grSelection.Bottom do
    for nCol := grSelection.Left to grSelection.Right do
      aCellsInfo[CellCoord(nRow, nCol)] := ciNewPlan;
  sgSchedule.Invalidate;
end;

procedure TfrmEditPgm.mnuNewPlanClick(Sender: TObject);
var
  newPlan : TPlan;
  miNewPlan : TMenuItem;
  nCount : Integer;
begin
  newPlan := AddPlan(True);
  if not (newPlan = nil) then
  begin
    nCount := 0;
    miNewPlan := nil;
    Repeat
      if (pmSchedule.Items[nCount].Caption = newPlan.Name) then miNewPlan := pmSchedule.Items[nCount];
      nCount := nCount + 1;
    Until not (miNewPlan = nil) or (nCount > pmSchedule.Items.Count-1);
    if not (miNewPlan = nil) then MenuClick(miNewPlan);
  end;
end;

//*  Fin Zone de Gestion de la Programmation ******

procedure TfrmEditPgm.FormCreate(Sender: TObject);
begin

//*  Zone de Gestion de la Programmation ******
  PrepareGrid();
  LoadCellsInfos();
  Application.OnShowHint := AppShowHint;
//*  Fin de Zone de Gestion de la Programmation ******

  InitImages();

  fPlans := TPlans.Create();

  //D�compresser l'archive dans le repertoire de travail
  PrepareWorkingFolder();
  //Charger les �l�ments (cf. cmbUnits)

  LoadPlans();

  //Une fois les travaux r�alis�s, sauver dans une archive avec la date
  //Renvoyer l'archive sur le ou les sites (suivant les unit�s positionn�s dans le fichier de configuration)
  seDelay.Value := CurrentDelay;

  EditMode := false;

end;

procedure TfrmEditPgm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  fPlans.Free();
  fLocalConfig.Free();
  fStoreParameters.Free();
end;

procedure TfrmEditPgm.DisplayPlans();
var
  nCount : Integer;
begin
  lbPlans.Clear();
  for nCount := 0 to fPlans.Count-1 do
    lbPlans.Items.Add(fPlans[nCount].Name);
end;

function TfrmEditPgm.GetMenuCellInfo(_MenuItem : TMenuItem) : TCellInfo;
begin
  result.BkColor := LocalColors[_MenuItem.Tag].Value;
  result.Plan := ReplaceString(_MenuItem.Caption, '&', '');
end;

procedure TfrmEditPgm.DisplayScheduler(_Scheduler : TScheduler);
var
  nCol, nRow,
  nHour,
  nCount : Integer;
begin
  for nCount := 0 to _Scheduler.Count-1 do
  begin
    nRow := _Scheduler[nCount].Day;
    nHour := SplitDate(_Scheduler[nCount].Hour).Hours;
    for nCol := nHour+1 to sgSchedule.ColCount-1 do
      aCellsInfo[CellCoord(nRow, nCol)] := GetMenuCellInfo(pmSchedule.Items[fPlans.IndexOf(_Scheduler[nCount].Plan)]);
  end;
  sgSchedule.Invalidate;
end;

procedure TfrmEditPgm.lbPlansClick(Sender: TObject);
var
  Plan : TPlan;
  sPath : String;
begin

  Plan := CurrentPlan;
  if not (Plan = nil) then
  begin
    sPath := CurrentPlan.Path;
    gbMonitors.Visible := DirectoryExists(sPath) and DisplayMonitors(sPath);
    bbtnLoadMultipleFiles.Visible := gbMonitors.Visible;
    if DirectoryExists(sPath) and not gbMonitors.Visible then ShowMessage('Aucun fichier Monitor.csv trouv� dans ' + SPath);
  end;

end;

function TfrmEditPgm.GetCurrentPlan() : TPlan;
begin
  Result := fPlans.GetPlanByName(ActualValue(lbPlans))
end;

procedure TfrmEditPgm.SaveLastLocation();
begin
  fParameters.Save();
end;

// Zone li�e aux moniteurs cf. hBoardLocal v1.4 //

function TfrmEditPgm.GetCurrentMonitor() : TMonitor;
begin
  if CurrentPlan.Monitors.Count >= fCurrentMonitor+1 then
    Result := CurrentPlan.Monitors[fCurrentMonitor]
  else
    Result := nil;
end;

procedure TfrmEditPgm.MonitorClick(Sender: TObject);
begin

  fCurrentMonitor := TWinControl(Sender).Tag;
  Page := 1;
  RefreshMonitors();

end;

procedure TfrmEditPgm.SelectAndCopyOneFile(_Image : TImage; _NewImage : Boolean = False);
var
  sFolder : String;
  nCount : Integer;
  function DuplicateFileName(_FileName : String) : Boolean;
  var
    nCountFileName : Integer;
  begin
    nCountFileName := CurrentMonitor.MediaList.Count-1;
    while
      (nCountFileName >= 0) and
      not (ExtractFileName(CurrentMonitor.MediaList[nCountFileName]) = ExtractFileName(_FileName)) do
      Dec(nCountFileName);
    Result := not (nCountFileName = -1);
  end;
begin
  odFichiers.FileName := fParameters.LastPicturesLocation + '*.PNG';
  odFichiers.InitialDir := fParameters.LastPicturesLocation;
  odFichiers.Options := odFichiers.Options - [ofAllowMultiSelect];
  if odFichiers.Execute then
    if not DuplicateFileName(odFichiers.FileName) then
    begin
      EditMode := True;
      sFolder := CurrentMonitor.MediaPath;
      for nCount := 0 to CurrentMonitor.MediaList.Count-1 do
        PascalCopyfile(CurrentMonitor.MediaList[nCount], AddSlash(sFolder) + ExtractFileName(CurrentMonitor.MediaList[nCount]));
      if not _NewImage then
        DeleteFile(AddSlash(sFolder) + ExtractFileName(CurrentMonitor.MediaList[(fPage - 1) * CST_MAX_IMAGES_PER_PAGE + _Image.Tag]));
      PascalCopyfile(odFichiers.FileName, AddSlash(sFolder) + ExtractFileName(odFichiers.FileName));
      CurrentMonitor.Media := AddSlash(sFolder) + ExtractFileName(CurrentMonitor.Media);
      RefreshMonitors();
      DisplayImages();
      fParameters.LastPicturesLocation := ExtractFilePath(odFichiers.FileName);
      SaveLastLocation();
    end
    else
      ShowMessage('Le fichier s�lectionn� poss�de un nom identique � un des fichiers actuels. Recopie impossible...');
end;

procedure TfrmEditPgm.SelectAndCopyMultipleFiles();
var
  nCount : Integer;
  sFolder : String;
begin
  odFichiers.FileName := fParameters.LastPicturesLocation + '*.PNG';
  odFichiers.InitialDir := fParameters.LastPicturesLocation;
  odFichiers.Options := odFichiers.Options + [ofAllowMultiSelect];
  if odFichiers.Execute then
  begin
    Screen.Cursor := crHourGlass;
    EditMode := True;
    sFolder := CurrentMonitor.MediaPath;
    for nCount := 0 to odFichiers.Files.Count-1 do
      PascalCopyfile(odFichiers.Files[nCount], AddSlash(sFolder) + ExtractFileName(odFichiers.Files[nCount]));
    CurrentMonitor.Media := AddSlash(sFolder) + ExtractFileName(CurrentMonitor.Media);
    RefreshMonitors();
    Page := 1;
    Screen.Cursor := crDefault;
    fParameters.LastPicturesLocation := ExtractFilePath(odFichiers.FileName);
    SaveLastLocation();
  end;
end;

procedure TfrmEditPgm.bbtnAddImageClick(Sender: TObject);
begin
  SelectAndCopyOneFile(TImage(Sender), true);
end;

procedure TfrmEditPgm.OnDeleteImageClick(Sender: TObject);
begin
  DeleteImage(TSpeedButton(Sender).Tag);
end;

procedure TfrmEditPgm.DeleteImage(_ImageIndex : Integer);
var
  sFolder : String;
  nCount : Integer;
begin
  if MessageDlg('D�sirez vous supprimer cette image ?', mtConfirmation, mbOKCancel, 0) = mrOk then
  begin
    EditMode := True;
    sFolder := CurrentMonitor.MediaPath;
    for nCount := 0 to CurrentMonitor.MediaList.Count-1 do
      if not (nCount = (fPage - 1) * CST_MAX_IMAGES_PER_PAGE + _ImageIndex) then
        PascalCopyfile(CurrentMonitor.MediaList[nCount], AddSlash(sFolder) + ExtractFileName(CurrentMonitor.MediaList[nCount]))
      else
        if FileExists(AddSlash(sFolder) + ExtractFileName(CurrentMonitor.MediaList[nCount])) then
          DeleteFile(AddSlash(sFolder) + ExtractFileName(CurrentMonitor.MediaList[nCount]));
    CurrentMonitor.Media := AddSlash(sFolder) + ExtractFileName(CurrentMonitor.Media);
    RefreshMonitors();
    Page := Minimum(MaxPages, fPage);
  end;
end;

procedure TfrmEditPgm.CheckNavigation();
begin
  bbtnPrevious.Enabled := fPage > 1;
  bbtnNext.Enabled := fPage < MaxPages;
end;

procedure TfrmEditPgm.InitImages();
begin
  lstImages := TList.Create();
  lstDeleteButtons := TList.Create();
  lstImages.Add(Image1);
  lstImages.Add(Image2);
  lstImages.Add(Image3);
  lstImages.Add(Image4);
  lstImages.Add(Image5);
  lstImages.Add(Image6);
  lstDeleteButtons.Add(sbDeleteImage1);
  lstDeleteButtons.Add(sbDeleteImage2);
  lstDeleteButtons.Add(sbDeleteImage3);
  lstDeleteButtons.Add(sbDeleteImage4);
  lstDeleteButtons.Add(sbDeleteImage5);
  lstDeleteButtons.Add(sbDeleteImage6);
end;

procedure TfrmEditPgm.ImagesClick(Sender: TObject);
begin
  SelectAndCopyOneFile(TImage(Sender));
end;

function TfrmEditPgm.IsImage(_FileName : String) : boolean;
begin
  _FileName := LowerCase(_FileName);
  result := (ExtractOnlyFileExt(_FileName) = 'jpg') or (ExtractOnlyFileExt(_FileName) = 'jpeg') or
            (ExtractOnlyFileExt(_FileName) = 'png') or (ExtractOnlyFileExt(_FileName) = 'bmp');
end;

procedure TfrmEditPgm.DisplayImages;
var
  sImageFileName : String;
  nCount : Integer;
  CurrentImage : TImage;
begin
  Screen.Cursor := crHourGlass;
  nCount := 0;
  while (nCount <= CST_MAX_IMAGES_PER_PAGE-1) do
  begin
    CurrentImage := lstImages[nCount];
    if (nCount + (fPage - 1) * CST_MAX_IMAGES_PER_PAGE <= CurrentMonitor.MediaList.Count-1) then
    begin
      sImageFileName := CurrentMonitor.MediaList[nCount + (fPage - 1) * CST_MAX_IMAGES_PER_PAGE];
      CurrentImage.Enabled := IsImage(sImageFileName);
    end
    else
      CurrentImage.Enabled := False;
    if CurrentImage.Enabled then
      DisplayImage(CurrentImage, CurrentMonitor.MediaList[nCount + (fPage - 1) * CST_MAX_IMAGES_PER_PAGE])
    else
      ClearImage(CurrentImage);
    TSpeedButton(lstDeleteButtons[nCount]).Visible := CurrentImage.Enabled;
    Inc(nCount);
  end;
  CheckNavigation();
  Screen.Cursor := crDefault;
end;

procedure TfrmEditPgm.bbtnPreviousClick(Sender: TObject);
begin
  if Page > 1 then Page := Page - 1;
end;

procedure TfrmEditPgm.bbtnNextClick(Sender: TObject);
begin
  if Page < MaxPages then Page := Page + 1;
end;

function TfrmEditPgm.MaxPages() : Integer;
begin
  if CurrentMonitor.MediaList.Count = 0 then
    result := 1
  else
    Result := (CurrentMonitor.MediaList.Count-1) DIV CST_MAX_IMAGES_PER_PAGE + 1;
end;

procedure TfrmEditPgm.SetPage(const Value: Integer);
begin
  if (Value >= 0) and (Value <= MaxPages) then
  begin
    fPage := Value;
    DisplayImages();
    lblMediasCount.Caption := IntToStr(fPage) + '/' + IntToStr(MaxPages);
  end;
  CheckNavigation();
end;

procedure TfrmEditPgm.ClearImage(_Image : TImage);
begin
  _Image.Picture := nil;
  TPanel(_Image.Parent).BevelOuter := bvNone;
end;

procedure TfrmEditPgm.DisplayImage(_Image : TImage; _ImageFile : String);
var
  pctMain : TPicture;
  dblRatioTarget,
  dblRatioSource : double;
begin
  if FileExists(_ImageFile) then
  begin

    lblTips.visible := False;

    pctMain:=TPicture.Create;
    pctMain.LoadFromFile(_ImageFile);

    dblRatioSource := pctMain.Width/pctMain.Height;
    dblRatioTarget := _Image.Parent.Width/_Image.Parent.Height;

    if (dblRatioTarget > dblRatioSource) then
    begin
      _Image.Left   := 0 + (_Image.Parent.Width - Round(dblRatioSource * _Image.Parent.Height)) div 2;
      _Image.Width  := Round(dblRatioSource * _Image.Parent.Height);
      _Image.Top    := 0;
      _Image.Height := _Image.Parent.Height;
    end
    else
    begin
      _Image.Left   := 0;
      _Image.Width  := _Image.Parent.Width;
      _Image.Top    := 0 + (_Image.Parent.Height - Round(_Image.Parent.Width / dblRatioSource)) div 2;
      _Image.Height := Round(_Image.Parent.Width / dblRatioSource);
    end;

    TPanel(_Image.Parent).BevelOuter := bvLowered;

    _Image.Stretch := not (ExtractOnlyFileExt(_ImageFile) = 'png');
    if not _Image.Stretch then
      Stretch(_Image.Width, _Image.Height, sfHermite, 0, pctMain.Bitmap, _Image.Picture.Bitmap)
    else
      _Image.Picture.LoadFromFile(_ImageFile);
    _Image.Hint := _ImageFile;
//    gbMonitors.Caption := CST_DEFAULT_MONITORS_CAPTION + ' ' + CurrentMonitor.CurrentMedia;

    pctMain.Free();

  end;

end;

procedure TfrmEditPgm.btnLoadPicturesClick(Sender: TObject);
begin
  SelectAndCopyMultipleFiles();
end;

{$WARNINGS OFF}
function TfrmEditPgm.DisplayMonitors(_Path : String) : Boolean;
var
  sbLastButton : TSpeedButton;
  nCount : Integer;
  procedure ClearMonitorsButtons();
  var
    nCount : integer;
  begin
    for nCount := gbMonitors.ControlCount-1 downto 0 do
      if (gbMonitors.Controls[nCount] is TSpeedButton) and
          (Copy(TSpeedButton(gbMonitors.Controls[nCount]).Name, 1, 8) = 'Monitor_') then
        gbMonitors.Controls[nCount].Free();
  end;
  function CreateMonitorButton(_Index : Integer; _Description : String) : TSpeedButton;
  var
    sbButton : TSpeedButton;
  begin
    sbButton := TSpeedButton.Create(Self);
    sbButton.Parent := gbMonitors;
    sbButton.Left := 8;
    sbButton.Top := 23 + 49 * _Index;
    sbButton.Width := 45;
    sbButton.Height := 45;
    sbButton.GroupIndex := 1;
    sbButton.Down := (_Index = 0);
    sbButton.Caption := Iif(_Description='', IntToStr(_Index + 1), _Description);
    sbButton.Name := 'Monitor_' + IntToStr(_Index);
    sbButton.Tag := _Index;
    sbButton.OnClick := MonitorClick;
    Result := sbButton;
  end;
begin
  ClearMonitorsButtons();
  Result := not (CurrentPlan.Monitors.Count = 0);
  if Result then
  begin
    for nCount := CurrentPlan.Monitors.Count-1 downto 0 do
      sbLastButton := CreateMonitorButton(nCount, CurrentPlan.Monitors[nCount].Description);
    sbLastButton.Click();
  end;
end;
{$WARNINGS ON}

// Fin de Zone li�e aux moniteurs //

procedure TfrmEditPgm.SavePlans();
var
  nDay, nHour : Integer;
  sPlanName,
  sPreviousPlanName : String;
begin
  fPlans.Scheduler.Clear();
  for nDay := 1 to 7 do
  begin
    sPreviousPlanName := '';
    for nHour := 0 to 23 do
    begin
      sPlanName := aCellsInfo[CellCoord(nDay, nHour+1)].Plan;
      if (sPreviousPlanName <> sPlanName) and not (sPlanName = '') then
      begin
        if not (fPlans.GetPlanByName(sPlanName) = nil) then
          fPlans.Scheduler.Add(TSchedule.Create(nDay, EncodeTime(nHour, 0, 0, 0), fPlans.GetPlanByName(sPlanName)))
        else
          fPlans.Scheduler.Add(TSchedule.Create(nDay, EncodeTime(nHour, 0, 0, 0), nil));
        sPreviousPlanName := sPlanName;
      end
    end;
  end;
  fPlans.Save(False);
end;

procedure ClearFolder(_FolderName : String);
begin
  if not (_FolderName = '') and DirectoryExists(_FolderName) then
    DeleteDirectory(
      _FolderName,
      false
      {$IFDEF DEBUG}, true{$ENDIF}
      );
end;

procedure TfrmEditPgm.ClearWorkingFolder();
begin
  ClearFolder(fParameters.TemporaryFolder);
end;

procedure TfrmEditPgm.Save();
var
  sDate : String;
begin
  DateTimeToString(sDate, 'dd/mm/yyyy', dtDate.Date);
  if (not FileExists(GetArchiveName())) or
     (MessageDlg('Une programmation existe d�j� pour la date du ' + sDate + '. Etes vous s�r(e) de vouloir l''�craser ?', mtWarning, mbOKCancel , 0) = mrOk) then
  begin
    SavePlans();
    frmZipAndUnzip := TfrmZipAndUnzip.Create(Self);
    frmZipAndUnzip.CreateArchive(fParameters.TemporaryFolder,
                                    UnitPath,
                                    RelToAbs(GetArchiveName(), ExtractFilePath(Application.ExeName)),
                                    'Sauvegarde des travaux en cours...');
    frmZipAndUnzip.Free();
    EditMode := false;
  end;
  if EditMode then Showmessage('La sauvegarde n''a pas �t� r�alis�e');
end;

function TfrmEditPgm.GetArchiveName() : String;
begin
  DateTimeToString(Result, 'yyyymmdd', dtDate.Date);
  Result := UnitPath + Result + '.zip';
end;

procedure TfrmEditPgm.bbtnSaveClick(Sender: TObject);
begin
  if (MessageDlg('Ces plans seront sauvegard�s puis envoy�s � tous les magasins concern�s (' + IntToStr(fStoreParameters.StoresDataLocation.Count) + ' magasins), voulez-vous continuer ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    Save();
    if not SendPlans
    (GetArchiveName(), fStoreParameters) then
      EditMode := True
    else
      ShowMessage('Envoi effectif !');
  end;
end;

procedure TfrmEditPgm.SetEditMode(const Value: Boolean);
begin
  if not (Value = fEditMode) then
  begin
    nLastPlanSelected := lbPlans.ItemIndex;
    fEditMode := Value;
    bbtnSave.Enabled := fEditMode;
    Caption := 'Gestion hBoard' + iif(fEditMode, ' - Mode Edition', '');
  end;
end;

procedure TfrmEditPgm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  nResult : Word;
begin
  nResult := mrNone;
  if EditMode then
    nResult := MessageDlg('Des modifications ont �t� effectu�es, Voulez vous sauvegarder et les envoyer aux magasins ?', mtConfirmation, mbYesNoCancel, 0);
  Case nResult of
    mrYes :
      Save();
    mrNo :
      EditMode := not (MessageDlg('Ces travaux seront perdus, voulez-vous continuer ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes);
  end;
  CanClose := not EditMode;
  if CanClose then ClearWorkingFolder();
end;

procedure TfrmEditPgm.RefreshMonitors();
begin
  gbMonitors.Caption := 'Ecrans : ' + ExtractDeepestDir(CurrentMonitor.Media);
end;

procedure TfrmEditPgm.bbtnLoadMultipleFilesClick(Sender: TObject);
begin
  SelectAndCopyMultipleFiles();
end;

procedure TfrmEditPgm.bbtnEditUnitClick(Sender: TObject);
begin
{*
var
  frmEditMonitors : TfrmEditMonitors;
begin
  if _Schedule <> nil then
  begin
    frmScheduleEditor := TfrmScheduleEditor.Create(self, fPlans, _Schedule);
    Result := frmScheduleEditor.ShowModal() = mrOk;
  end
  else
    Result := False;
end;
*}
end;

procedure TfrmEditPgm.mnuDeleteCurrentPlanClick(Sender: TObject);
begin
  DeleteCurrentPlan();
end;

procedure TfrmEditPgm.mnuAddPlanClick(Sender: TObject);
begin
  AddPlan(False);
end;

function TfrmEditPgm.AddPlan(_AffectNewPlan : Boolean) : TPlan;
var
  sNewPlanName : String;
  nResult : Integer;
  procedure CreateMonitorFile(_RootFolder : String; _ScreenList : TStringList);
  var
    lstOutput : TStringList;
    nCountScreen : Integer;
  begin
    lstOutput := TStringList.Create();
    lstOutput.Add(CST_FILE_MONITOR_PREFIX);
    for nCountScreen := 0 to _ScreenList.Count-1 do
      lstOutput.Add
        (
          IntToStr(nCountScreen) + ';' +
          _ScreenList[nCountScreen] +
          CST_FILE_MONITOR_SUFFIX
        );
    lstOutput.SaveToFile(AddSlash(_RootFolder) + CST_MONITOR_FILENAME);
    lstOutput.Free();
  end;
  function CreateFolderPlan(_PlanName : String; _Folder : String; _ScreenList : TStringList) : Boolean;
  var
    nCountScreen : Integer;
    sNewPlanFolder : String;
  begin
    sNewPlanFolder := AddSlash(_Folder) + AddSlash(_PlanName);
    Result := CreateDir(sNewPlanFolder);
    if Result then
      for nCountScreen := 0 to _ScreenList.Count-1 do
        Result := Result and CreateDir(sNewPlanFolder + _ScreenList[nCountScreen]);
    if Result then
      CreateMonitorFile(sNewPlanFolder, _ScreenList)
  end;
begin
  Result := nil;
  frmNewPlan := TfrmNewPlan.Create(Self, lbPlans.Items);
  nResult := frmNewPlan.ShowModal();
  if nResult = mrOk then
  begin
    EditMode := True;
    sNewPlanName := frmNewPlan.NewPlanName;
    if not CreateFolderPlan(sNewPlanName, fParameters.TemporaryFolder, fStoreParameters.Screens) then
    begin
      try
        DeleteDirectory(fParameters.TemporaryFolder + AddSlash(sNewPlanName));
      except
      end;
      ShowMessage('Une erreur est intervenue lors de la cr�ation du r�pertoire ' + fParameters.TemporaryFolder +
                  ' concernant le plan ' + sNewPlanName +
                  '. Veuillez appeler votre conseiller technique pour d�terminer la cause du probl�me');
    end
    else
    begin
      Result := fPlans[fPlans.Add(TPlan.Create(sNewPlanName, fParameters.TemporaryFolder + sNewPlanName, fParameters.TemporaryFolder))];
      Result.Tag := GetNewPlanColorIndex;
      if _AffectNewPlan then AffectPlan(GetNewPlanColorIndex, sNewPlanName);
      SavePlans();
      LoadPlans(sNewPlanName);
    end;
  end;
  frmNewPlan.Free();
end;

procedure TfrmEditPgm.DeleteCurrentPlan();
var
  PlanToDelete : TPlan;
  nDay, nHour : Integer;
begin
  PlanToDelete := fPlans.GetPlanByName(ActualValue(lbPlans));
  if not (CurrentPlan = nil) and
     (MessageDlg('Etes vous s�r(e) de vouloir supprimer le plan ' + PlanToDelete.Name + ' et toutes les programmations associ�es ? Cette op�ration est irr�versible !', mtWarning, mbOKCancel, 0) = mrOk) then
  begin
    EditMode := True;
    DeleteDirectory(PlanToDelete.Path, True);
    for nDay := 1 to 7 do
      for nHour := 0 to 23 do
        if aCellsInfo[CellCoord(nDay, nHour+1)].Plan = PlanToDelete.Name then
        begin
          aCellsInfo[CellCoord(nDay, nHour+1)].BkColor := sgSchedule.Color;
          aCellsInfo[CellCoord(nDay, nHour+1)].Plan := '';
        end;
    SavePlans();
    LoadPlans();
  end;
end;

procedure TfrmEditPgm.mnuDeleteCurrentImageClick(Sender: TObject);
begin
  DeleteImage(TImage(TPopupMenu(TMenuItem(Sender).GetParentMenu).PopupComponent).Tag);
end;

procedure TfrmEditPgm.mnuAddImageClick(Sender: TObject);
begin
  SelectAndCopyOneFile(TImage(TPopupMenu(TMenuItem(Sender).GetParentMenu).PopupComponent));
end;

procedure TfrmEditPgm.SetCurrentDelay(const Value: Integer);
begin
  fLocalConfig.RotationDelay := Value;
end;

function TfrmEditPgm.GetCurrentDelay : Integer;
begin
  Result := fLocalConfig.RotationDelay;
end;

procedure TfrmEditPgm.seDelayChange(Sender: TObject);
begin
  If isNum(seDelay.Text) then
  begin
    CurrentDelay := seDelay.Value;
    EditMode := True;
  end;
end;

constructor TfrmEditPgm.Create(_Owner: TComponent; _Parameters: TParameters; _UnitDetail : TUnit; _UnitPath, _UnitArchive : String);
var
  dMinDate : TDateTime;
  nYear,
  nMonth,
  nDay : Word;
begin
  inherited Create(_Owner);
  fParameters := _Parameters;
  fLocalConfig := TLocalConfig.Create(_UnitPath);
  fStoreParameters := TStoresParameters.Create(_UnitPath);

  UnitDetail := _UnitDetail;
  UnitPath := _UnitPath;
  UnitArchive := _UnitArchive;

  dtDate.Date := EncodeDateTime(StrToInt(Copy(UnitArchive, 1, 4)),
                                StrToInt(Copy(UnitArchive, 5, 2)),
                                StrToInt(Copy(UnitArchive, 7, 2)), 0, 0, 0, 0);
  DecodeDate(Now(), nYear, nMonth, nDay);
  dMinDate := EncodeDate(nYear, nMonth, nDay);
  dtDate.Date := Max(dtDate.Date, dMinDate);
  dtDate.MinDate := dMinDate;

end;

procedure TfrmEditPgm.PrepareWorkingFolder();
var
  frmZipAndUnzip : TfrmZipAndUnzip;
begin
  ClearWorkingFolder();
  frmZipAndUnzip := TfrmZipAndUnzip.Create(Self);
  frmZipAndUnzip.LoadArchive(AddSlash(UnitPath) + UnitArchive, fParameters.TemporaryFolder, 'Mise en place de l''environnement en cours...');
  frmZipAndUnzip.Free();
end;

function TfrmEditPgm.GetNewPlanColorIndex() : Integer;
begin
  Result := (fPlans.Count-1) Mod (High(LocalColors)-(Low(LocalColors)+1) ) + 2;
end;

procedure TfrmEditPgm.LoadPlans(_ForcePlanToSelect : String = '');

  procedure AddUnscheduledPlans();
  var
    srResult : TSearchRec;
    nRet : Integer;
  begin
    nRet := FindFirst(fParameters.TemporaryFolder + '*.*', faDirectory, srResult);
    while (nRet = 0) do
    begin
      if ((srResult.attr and faDirectory) = faDirectory) and (fPlans.GetPlanByName(srResult.Name) = nil) and not (srResult.Name[1] = '.') then
        fPlans.Add(TPlan.Create(srResult.Name, fParameters.TemporaryFolder + srResult.Name, fParameters.TemporaryFolder));
      nRet := FindNext(srResult);
    end;
  end;
  procedure AffectColorsToPlans();
  var
    nCountColors,
    nCountPlans : Integer;
  begin
    // First Two Colors affected to NewPlan and Standby
    fPlans[0].Tag := 1;
    nCountColors := Low(LocalColors)+2;
    for nCountPlans := 1 to fPlans.Count-1 do
    begin
      fPlans[nCountPlans].Tag := nCountColors;
      nCountColors := nCountColors + 1;
      if nCountColors > High(LocalColors) then nCountColors := Low(LocalColors);
    end;
  end;
begin

  fPlans.AutoCreateSleepPlan := True;
  fPlans.Path := fParameters.TemporaryFolder;
  fPlans.ForceRootPath(fParameters.TemporaryFolder);
  AddUnscheduledPlans();
  AffectColorsToPlans();

  PreparePopup();
  DisplayPlans();
  if not (_ForcePlanToSelect = '') then nLastPlanSelected := lbPlans.Items.IndexOf(_ForcePlanToSelect);
  DisplayScheduler(fPlans.Scheduler);
  SelectItem(lbPlans, nLastPlanSelected);

end;

procedure TfrmEditPgm.SetUnitArchive(const Value: String);
begin
  fUnitArchive := Value;
end;

procedure TfrmEditPgm.SetUnitDetail(const Value: TUnit);
begin
  fUnitDetail := Value;
end;

procedure TfrmEditPgm.SetUnitPath(const Value: String);
begin
  fUnitPath := AddSlash(Value);
end;

function TfrmEditPgm.GetRootFolder() : String;
begin
  result := fParameters.TemporaryFolder;
end;

procedure TfrmEditPgm.lbPlansDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  bmpDisplay :TBitmap;
begin
  bmpDisplay := TBitmap.Create;
  with (Control as TListBox) do
  begin
    if Index < ImageList.Count then ImageList.GetBitmap(fPlans.GetPlanByName(Items[Index]).Tag, bmpDisplay);
    Canvas.FillRect(Rect);
    Canvas.TextOut(Rect.Left+ImageList.Height+2,Rect.Top,Items[Index]);
    Canvas.Draw(Rect.Left+1,Rect.Top+1,bmpDisplay);
  end;
  bmpDisplay.Free;
end;

end.

