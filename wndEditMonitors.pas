unit wndEditMonitors;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uHBoardUtils, StdCtrls, CheckLst;

type
  TfrmEditMonitors = class(TForm)
    clbStores: TCheckListBox;
  private
    fParameters : TParameters;
    function LoadStores(_Path : String; _IPAddress : TStringList = nil; _PgmLocation : TStringList = nil) : TStringList;
    procedure CheckStores(_ScreensOrgPath: String);
  public
    constructor Create(_Owner: TComponent; _Parameters: TParameters; _UnitPath, _ScreensOrgPath : String); overload;
    destructor Destroy; override;
  end;

  TStore = record
    Name : String;
    IPAddress : String;
    PgmLocation : String;
  end;
var
  frmEditMonitors: TfrmEditMonitors;

implementation

uses iniFiles, uCommon;

Const
  CST_STORES_INI_FILENAME = 'magasins.ini';
  CST_SCREENS_INI_FILENAME = 'ecrans.ini';
  CST_STORES_SECTION = 'Magasins';

{$R *.dfm}
constructor TfrmEditMonitors.Create(_Owner : TComponent; _Parameters : TParameters; _UnitPath, _ScreensOrgPath : String);
var
  lstStores : TStringList;
begin
  Inherited Create(_Owner);
  fParameters := _Parameters;
  lstStores := LoadStores(_UnitPath);
  clbStores.Items.Assign(lstStores);
  CheckStores(_ScreensOrgPath);
  lstStores.Free();
end;

destructor TfrmEditMonitors.Destroy();
begin
  Inherited Destroy();
end;

function TfrmEditMonitors.LoadStores(_Path : String; _IPAddress : TStringList = nil; _PgmLocation : TStringList = nil) : TStringList;
var
  iniFile : TiniFile;
begin
  result := TStringList.Create();
  iniFile := TiniFile.Create(AddSlash(_Path) + CST_STORES_INI_FILENAME);
  iniFile.ReadSection(CST_STORES_SECTION, result);
  iniFile.Free();
end;

procedure TfrmEditMonitors.CheckStores(_ScreensOrgPath : String);
var
  lstScreensOrgSelection : TStringList;
begin
  lstScreensOrgSelection := TStringList.Create;
  GetAllFolders(_ScreensOrgPath, lstScreensOrgSelection);
  lstScreensOrgSelection.Free();
end;

end.
