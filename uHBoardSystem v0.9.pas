unit uHBoardSystem;

interface

uses Classes, Graphics;

type
  TOrientation =(OHorizontal,OVertical);
  TStretchMode =(smNormal,smHeight, smWidth);

  TMonitor = class(TObject)
    fIndex : Integer;
    fWidth : Integer;
    fHeight : Integer;
    fMedia : String;
    fMediaIndex : Integer;
    fOrientation : TOrientation;
    fStretchMode : TStretchMode;
    fDescription : String;
  private
    fMediaList : TStringList;
    procedure SetMedia(const _Value : String);
    function GetCurrentMedia: String;
    function GetCurrentMediaIndex: Integer;
  public
    constructor Create(_Index : Integer; _Media : String; _StretchMode : TStretchMode; _Description : String; _Width, _Height : Integer; _Orientation : TOrientation); overload;
    constructor Create(_Index : Integer; _Media : String; _StretchMode : TStretchMode; _Description : String); overload;
    constructor Create(); overload;
    destructor Destroy; override;
    function FirstMedia : String;
    function NextMedia : String;
    function PreviousMedia: String;
    function MediasCount : Integer;
    property Index : Integer read FIndex write FIndex;
    property Width : Integer read FWidth write FWidth;
    property Height : Integer read FHeight write FHeight;
    property StretchMode : TStretchMode read fStretchMode write fStretchMode;
    property Orientation : TOrientation read FOrientation write FOrientation;
    property Description : String read FDescription write FDescription;
    property Media : String read FMedia write SetMedia;
    property MediaIndex : Integer read GetCurrentMediaIndex;
    property CurrentMedia : String read GetCurrentMedia;
  end;

  TMonitors = class(TList)
  private
    fRead : Boolean;
    fPath: String;
    fRootPath: String;
    procedure SetPath(const Value: String);
    procedure ReadMonitors();
    function GetMonitor(_Index : Integer) : TMonitor;
    procedure SetRootPath(const Value: String);
    function GetMonitorPath() : String;
  public
    constructor Create; overload;
    destructor Destroy; override;
    procedure Clear; override;
    function Add(_Monitor : TMonitor) : Integer; overload;
    property RootPath: String read fRootPath write SetRootPath;
    property Path: String read fPath write SetPath;
    property Monitor[Index:Integer]:TMonitor read GetMonitor; default;
  end;

  TPlan = class(TObject)
    fPath : String;
    fName : String;
    fMonitors : TMonitors;
  private
    procedure ReloadMonitors;
    procedure SetName(const Value: String);
    procedure SetPath(const Value: String);
  public
    constructor Create(_Name : String; _Folder: String); overload;
    destructor Destroy; override;
    property Path : String read fPath write SetPath;
    property Name : String read fName write SetName;
    property Monitors : TMonitors read fMonitors;
  end;

  TSchedule = class(TObject)
    fDay : Integer;
    fHour : TDateTime;
    fPlan : TPlan;
  public
    constructor Create(); overload;
    constructor Create(_Day: Integer; _Hour: TDateTime; _Plan: TPlan); overload;
    property Day : Integer read fDay write fDay;
    property Hour : TDateTime read fHour write fHour;
    property Plan : TPlan read fPlan write fPlan;
  end;

  TScheduler = class(TList)
  private
    function GetSchedule(_Index : Integer) : TSchedule;
  public
    constructor Create();
    destructor Destroy; override;
    procedure Clear; override;
    function Add(_Schedule : TSchedule) : Integer; overload;
    property Schedule[Index:Integer]:TSchedule read GetSchedule; default;
  end;

  TPlans = class(TList)
  private
    fPath: String;
    fDefaultPlan : TPlan;
    fScheduler : TScheduler;
    procedure SetPath(const Value: String);
    procedure ReadPlans();
    function GetPlan(_Index : Integer) : TPlan;
  public
    constructor Create();
    destructor Destroy; override;
    procedure Clear; override;
    procedure ForceRootPath(_RootPath : String);
    function GetPlanByName(_Name : String) : TPlan;
    function Add(_Plan : TPlan) : Integer; overload;
    property Path: String read fPath write SetPath;
    property Plan[Index:Integer]:TPlan read GetPlan; default;
    property Scheduler : TScheduler read fScheduler;
  end;

function StrToStretchMode(_StretchMode : String) : TStretchMode;

implementation

uses SysUtils, uCommon, GraphicEx;

Const
  CST_PLAN_FILENAME = 'plan.csv';
  CST_MONITOR_FILENAME = 'monitor.csv';

{ Commons }
function LoadCSVInStrings(_FileName: string): TStringList;
var
  tsResult : TStringList;
begin
  tsResult:=TStringList.Create();
  tsResult.LoadFromFile(_FileName);
  if tsResult.Count>0 then
    while tsResult[tsResult.Count-1]='' do tsResult.Delete(tsResult.Count-1);
  result := tsResult;
end;

function GetCSVcell(_Line: string; _Index: integer): string;
var
  tsResult:TStringList;
begin
  tsResult:=TStringList.Create();
  tsResult.Text:=StringReplace(_Line,';',#13#10,[rfReplaceAll]);
  try
    if _Index <= tsResult.Count-1 then
      result:=tsResult[_Index]
    else
      result := '';
  except
    result:='';
  end;
  tsResult.Free();
end;

{ TPlans }

constructor TPlans.Create;
begin
  inherited Create();
  fScheduler := TScheduler.Create;
end;

destructor TPlans.Destroy;
begin
  Clear;
  fScheduler.free;
  fScheduler := nil;
  inherited;
end;

function TPlans.GetPlan(_Index: Integer): TPlan;
begin
  result := TPlan(Items[_Index]);
end;

procedure TPlans.Clear;
var
  nCount : Integer;
begin
  for nCount := Count-1 downto 0 do Plan[nCount].Free();
  if not (fDefaultPlan = nil) then fDefaultPlan.Free();
  fDefaultPlan := nil;
  if not (fScheduler = nil) then fScheduler.Clear();
  inherited;
end;

procedure TPlans.ReadPlans();
var
  sPlanName,
  sFileName : String;
  slReadPlans : TStringList;
  nCount : Integer;
  Plan : TPlan;
begin
  Clear();
  sFileName := AddSlash(Path) + CST_PLAN_FILENAME;
  if FileExists(sFileName) then
  begin
    fDefaultPlan := TPlan.Create('*', Path);
    slReadPlans := LoadCSVinStrings(sFileName);
    for nCount := 1 to slReadPlans.Count-1 do
    begin
      sPlanName := GetCSVcell(slReadPlans[nCount], 2);
      Plan := GetPlanByName(sPlanName);
      if (Plan = nil) then
      begin
        Plan := TPlan.Create(sPlanName, AddSlash(Path) + sPlanName);
        Add(Plan);
      end;
      fScheduler.Add(
        TSchedule.Create(
          StrToInt(GetCSVcell(slReadPlans[nCount], 0)),
          StrToTimeFormat(GetCSVcell(slReadPlans[nCount], 1), 'hh:nn:ss'),
          Plan
          )
        );
    end;
    slReadPlans.Free();
  end;
end;

procedure TPlans.SetPath(const Value: String);
begin
  fPath := Value;
  ReadPlans();
end;

function TPlans.Add(_Plan: TPlan): Integer;
begin
  result := inherited Add(_Plan);
end;

function TPlans.GetPlanByName(_Name : String): TPlan;
var
  nCount : Integer;
begin
  nCount := Count-1;
  While (nCount > -1) and not (Plan[nCount].Name = _Name) do
    nCount := nCount - 1;
  if nCount > -1 then
    result := Plan[nCount]
  else
    result := nil;
end;

procedure TPlans.ForceRootPath(_RootPath: String);
var
  nCount : Integer;
begin
  for nCount := 0 to Count-1 do
    Plan[nCount].Monitors.RootPath := _RootPath;
end;

{ TPlan }

constructor TPlan.Create(_Name, _Folder: String);
begin
  inherited Create();
  fMonitors := TMonitors.Create();
  fName := _Name;
  Path := _Folder;
end;

destructor TPlan.Destroy;
begin
  fMonitors.Free();
  inherited;
end;

procedure TPlan.ReloadMonitors;
begin
  fMonitors.Path := fPath;
end;

procedure TPlan.SetName(const Value: String);
begin
  fName := Value;
  ReloadMonitors();
end;

procedure TPlan.SetPath(const Value: String);
begin
  fPath := Value;
  ReloadMonitors();
end;

{ TMonitors }

function TMonitors.Add(_Monitor: TMonitor): Integer;
begin
  result := inherited Add(_Monitor);
end;

procedure TMonitors.Clear;
var
  nCount : Integer;
begin
  for nCount := Count-1 downto 0 do Monitor[nCount].Free();
  inherited;
end;

constructor TMonitors.Create;
begin
  inherited Create();
  fRead := False;
end;

destructor TMonitors.Destroy;
begin
  Clear;
  inherited;
end;

function TMonitors.GetMonitor(_Index: Integer): TMonitor;
begin
  result := TMonitor(Items[_Index]);
end;

procedure TMonitors.ReadMonitors();
var
  sMonitorFileName : String;
  slReadMonitors : TStringList;
  nCount : Integer;
begin
  Clear();
  sMonitorFileName := AddSlash(Path) + CST_MONITOR_FILENAME;
  if FileExists(sMonitorFileName) then
  begin
    slReadMonitors := LoadCSVinStrings(sMonitorFileName);
    for nCount := 1 to slReadMonitors.Count-1 do
      Add(
        TMonitor.Create(
          StrToInt(GetCSVcell(slReadMonitors[nCount], 0)),
          AddSlash(GetMonitorPath()) + GetCSVcell(slReadMonitors[nCount], 1),
          StrToStretchMode(GetCSVcell(slReadMonitors[nCount], 2)),
          GetCSVcell(slReadMonitors[nCount], 3))
        );
    slReadMonitors.Free();
  end;
  fRead := True;
end;

procedure TMonitors.SetPath(const Value: String);
begin
  fPath := Value;
  ReadMonitors();
end;

procedure TMonitors.SetRootPath(const Value: String);
begin
  fRootPath := Value;
  ReadMonitors();
end;

function TMonitors.GetMonitorPath : String;
begin
  if fRootPath = '' then
    result := fPath
  else
    result := fRootPath;
end;

{ TMonitor }

constructor TMonitor.Create(_Index : Integer; _Media : String; _StretchMode : TStretchMode; _Description : String; _Width, _Height : Integer; _Orientation : TOrientation);
begin
  Create(_Index, _Media, _StretchMode, _Description);
  Index := _Index;
  Width := _Width;
  Height := _Height;
  Orientation := _Orientation;
end;

constructor TMonitor.Create(_Index : Integer; _Media : String; _StretchMode : TStretchMode; _Description : String);
begin
  Create();
  Index := _Index;
  Media := _Media;
  StretchMode := _StretchMode;
  Description := _Description;
end;

constructor TMonitor.Create();
begin
  fMediaList := TStringList.Create();
  fMediaIndex := 0;
  fMedia := '';
  fWidth := 0;
  fHeight := 0;
  fOrientation := OHorizontal;
  fStretchMode := smNormal;
  inherited Create();
end;

destructor TMonitor.Destroy;
begin
  fMediaList.Free();
  inherited;
end;

function TMonitor.GetCurrentMedia: String;
begin
  if fMediaList.Count >= FMediaIndex+1 then
    Result := fMediaList[FMediaIndex]
  else
    Result := '';
end;

function TMonitor.FirstMedia: String;
begin
  fMediaIndex := 0;
  Result := GetCurrentMedia();
end;

function TMonitor.NextMedia: String;
begin                                                                                          
  if fMediaList.Count > FMediaIndex+1 then
    fMediaIndex := fMediaIndex + 1
  else
    fMediaIndex := 0;
  Result := GetCurrentMedia();
end;

function TMonitor.PreviousMedia: String;
begin
  if FMediaIndex > 0 then
    fMediaIndex := fMediaIndex - 1
  else
    fMediaIndex := fMediaList.Count-1;
  Result := GetCurrentMedia();
end;

procedure TMonitor.SetMedia(const _Value : String);
var
  srMedia : TSearchRec;
  sRoot : string;
begin
  fMedia := _Value;
  sRoot := ExtractFilePath(fMedia);
  fMediaList.Clear();
  if FindFirst(fMedia, faAnyFile, srMedia) = 0 then
  begin
    repeat
      fMediaList.Add(AddSlash(sRoot) + srMedia.Name);
    until FindNext(srMedia) <> 0;
  end;
  FindClose(srMedia);
  fMediaIndex := 0;
end;

function TMonitor.MediasCount: Integer;
begin
  result := fMediaList.Count;
end;

function TMonitor.GetCurrentMediaIndex: Integer;
begin
  result := fMediaIndex;
end;

function StrToStretchMode(_StretchMode : String) : TStretchMode;
begin
  if _StretchMode = '+W' then
    result := smWidth
  else if _StretchMode = '+H' then
    result := smHeight
  else
    result := smNormal
end;

{ TScheduler }

function TScheduler.Add(_Schedule: TSchedule): Integer;
begin
  result := inherited Add(_Schedule);
end;

procedure TScheduler.Clear;
var
  nCount : Integer;
begin
  for nCount := Count-1 downto 0 do Schedule[nCount].Free();
  inherited;
end;

constructor TScheduler.Create;
begin
  inherited;
end;

destructor TScheduler.Destroy;
begin
  Clear();
  inherited;
end;

function TScheduler.GetSchedule(_Index: Integer): TSchedule;
begin
  result := TSchedule(Items[_Index]);
end;

{ TSchedule }

constructor TSchedule.Create();
begin
  inherited Create();
  fDay := 0;
  fHour := 0;
  fPlan := nil;
end;

constructor TSchedule.Create(_Day: Integer; _Hour: TDateTime; _Plan: TPlan);
begin
  inherited Create();
  fDay := _Day;
  fHour := _Hour;
  fPlan := _Plan;
end;

end.
